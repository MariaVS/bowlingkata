﻿using System;
using System.Collections.Generic;

namespace ShopApp
{
    public class User : IObserver
    {
        public string Name { get; set; }
        private readonly List<Message> _message;
        private readonly ISubject _shop;

        public User(string name, ISubject shop)
        {
            Name = name;
            _shop = shop;
            _message = new List<Message>();
        }
        public void SendLetter(Message message)
        {
            _message.Add(message);
            Console.WriteLine($"I'm {Name}, get message at {DateTime.Now}, {message.Text}");
        }

        public void Subscribe(string productName)
        {
            _shop.RegisterObserver(this, productName);
        }

        public void Unsubscribe(string productName)
        {
            _shop.RemoveObserver(this, productName);
        }
    }
}
