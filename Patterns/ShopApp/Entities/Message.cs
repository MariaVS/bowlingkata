﻿using System;

namespace ShopApp
{
    public class Message
    {
        public DateTime Date { get; set; }
        public string Thema { get; set; }
        public string Text { get; set; }
    }
}
