﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShopApp
{
    public class Shop : ISubject
    {
        private readonly Dictionary<string, List<IObserver>> _observers;
        private readonly List<Product> _products;

        public Shop()
        {
            _observers = new Dictionary<string, List<IObserver>>();
            _products = new List<Product>();
        }
        public void NotifyObservers(Product product)
        {
            if (_observers.TryGetValue(product.Name, out var observers))
            {
                Message message = new Message()
                {
                    Date = DateTime.Now, 
                    Text = $"Received {product.Name} with price:{product.Price}",
                    Thema = $"{product.Name}"
                };
                foreach (var observer in observers)
                {
                    observer.SendLetter(message);
                }
            }
        }

        public void RegisterObserver(IObserver o, string productName)
        {
            if (_observers.ContainsKey(productName))
            {
                _observers[productName].Add(o);
            }
            else
            {
                _observers.Add(productName, new List<IObserver>() { o });
            }

            NotifyNewObserver(o, productName);
        }

        public void RemoveObserver(IObserver o, string productName)
        {
            if (_observers.ContainsKey(productName))
            {
                _observers[productName].Remove(o);
            }
        }

        public void AddProduct(Product product)
        {
            _products.Add(product);
            NotifyObservers(product);
        }

        public void RemoveProduct(Product product)
        {
            _products.Remove(product);
        }

        public void NotifyNewObserver(IObserver o, string productName)
        {
            var product = _products.FirstOrDefault(x => x.Name == productName);
            string text;
            if (product is not null)
            {
                text = $"Available {product.Name} with price:{product.Price}";
            }
            else
            {
                text = $"Sorry, your product {productName} not in stock";
            }

            Message message = new Message()
            {
                Date = DateTime.Now,
                Text = text,
                Thema = $"{productName}"
            };
            o.SendLetter(message);
        }
    }
}
