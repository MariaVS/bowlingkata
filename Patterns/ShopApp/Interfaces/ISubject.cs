﻿namespace ShopApp
{
    public interface ISubject
    {
        void RegisterObserver(IObserver o, string productName);
        void RemoveObserver(IObserver o, string productName);
        void NotifyObservers(Product product);
    }
}
