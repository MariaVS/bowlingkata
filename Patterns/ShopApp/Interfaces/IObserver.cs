﻿namespace ShopApp
{
    public interface IObserver
    {
        void SendLetter(Message message);
    }
}
