﻿using System;

namespace ShopApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Product book = new Product()
            {
                Name = "Book",
                Price = 21
            };
            Product game = new Product()
            {
                Name = "Game",
                Price = 30
            };
            Product laptop = new Product()
            {
                Name = "Laptop",
                Price = 300
            };

            Shop shop = new Shop();
            shop.AddProduct(laptop);

            User user = new User("Vlad", shop);
            User user2 = new User("Diana", shop);
            user.Subscribe(game.Name);
            shop.AddProduct(game);
            user2.Subscribe(game.Name);
            user2.Unsubscribe(game.Name);
            shop.RemoveProduct(game);
            shop.AddProduct(game);
            Console.ReadLine();
        }
    }
}
