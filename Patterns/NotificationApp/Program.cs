﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NotificationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            var types = config.GetSection("NotificationTypes").Get<List<NotificationType>>();
            
            var result = Constructor.Construct(types);
            result.Notify("Next Monday is holiday");
            Console.ReadLine();
        }
    }
}
