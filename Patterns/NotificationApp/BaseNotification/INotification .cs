﻿namespace NotificationApp
{
    public interface INotification
    {
        void Notify(string message);
    }
}
