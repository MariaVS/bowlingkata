﻿using System;

namespace NotificationApp
{
    public class Email : INotification
    {
        public void Notify(string message)
        {
            Console.WriteLine($"I send message on Email, it's {message}");
        }
    }
}
