﻿namespace NotificationApp
{
    public abstract class NotificationDecorator : INotification
    {
        protected INotification Notification;
        public abstract void Notify(string message);
    }
}
