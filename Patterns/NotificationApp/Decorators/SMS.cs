﻿using System;

namespace NotificationApp
{
    public class SMS : NotificationDecorator
    {
        public SMS(INotification notification)
        {
            Notification = notification;
        }
        public override void Notify(string message)
        {
            Notification.Notify(message);
            Console.WriteLine("I send message on SMS");
        }
    }
}
