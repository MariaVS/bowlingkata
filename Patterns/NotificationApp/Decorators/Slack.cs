﻿using System;

namespace NotificationApp
{
    public class Slack : NotificationDecorator
    {
        public Slack(INotification notification)
        {
            Notification = notification;
        }
        public override void Notify(string message)
        {
            Notification.Notify(message);
            Console.WriteLine("I send message on Slack");
        }
    }
}
