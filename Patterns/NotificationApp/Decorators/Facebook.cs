﻿using System;

namespace NotificationApp
{
    public class Facebook : NotificationDecorator
    {
        public Facebook(INotification notification)
        {
            Notification = notification;
        }
        public override void Notify(string message)
        {
            Notification.Notify(message);
            Console.WriteLine("I send message on Facebook");
        }
    }
}
