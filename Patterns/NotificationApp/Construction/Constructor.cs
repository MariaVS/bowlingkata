﻿using System.Collections.Generic;
using System.Linq;

namespace NotificationApp
{
    public static class Constructor
    {
        public static INotification Construct(List<NotificationType> notificationType)
        {
            INotification notification = new Email();
            var types = notificationType.Distinct();
            foreach (var type in types)
            {
                switch (type)
                {
                    case NotificationType.Facebook:
                        notification = new Facebook(notification);
                        break;
                    case NotificationType.SMS:
                        notification = new SMS(notification);
                        break;
                    case NotificationType.Slack:
                        notification = new Slack(notification);
                        break;
                    default:
                        break;
                }
            }
            return notification; 
        }
    }
}
