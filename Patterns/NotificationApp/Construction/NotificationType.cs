﻿
namespace NotificationApp
{
    public enum NotificationType
    {
        Facebook, 
        Slack, 
        SMS, 
        Email
    }
}
