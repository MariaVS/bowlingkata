﻿using System;
using BuilderHomeApp.Entities;

namespace BuilderHomeApp
{
    public class HomeBuilder : IBuilder
    {
        private Home _home;
        public HomeBuilder()
        {
            _home = new Home();
        }

        public void BuildDoor()
        {
            if (_home.Wall is null)
            {
                throw new ApplicationException("You can't build door without wall");
            }
            _home.Door = new Door();
        }

        public void BuildFoundation()
        {
            _home.Foundation = new Foundation();
        }

        public Home GetResult ()
        {
            if (_home.Foundation is null || _home.Wall is null || _home.Door is null || _home.Roof is null || _home.Window is null)
            {
                throw new ApplicationException("We have to make foundation, wall, door, roof, window");
            }
            return _home;
        }

        public void BuildRoof()
        {
            if (_home.Wall is null)
            {
                throw new ApplicationException("You can't build roof without wall");
            }
            _home.Roof = new Roof();
        }

        public void BuildWall()
        {
            if (_home.Foundation is null)
            {
                throw new ApplicationException("You can't build wall without foundation");
            }
            _home.Wall = new Wall();
        }

        public void BuildWindow()
        {
            if (_home.Wall is null)
            {
                throw new ApplicationException("You can't build window without wall");
            }
            _home.Window = new Window();
        }

        public void BuildWallPaper()
        {
            if (_home.Wall is null)
            {
                throw new ApplicationException("You can't build wall paper without wall");
            }
            _home.WallPaper = new WallPaper();
        }

        public void BuildFloor()
        {
            if (_home.Foundation is null)
            {
                throw new ApplicationException("You can't build floor without foundation");
            }
            _home.Floor = new Floor();

        }

        public void BuildPainting()
        {
            _home.Painting = new Painting();
        }
    }
}
