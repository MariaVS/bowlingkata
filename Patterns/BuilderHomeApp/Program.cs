﻿using System;
using System.Collections.Generic;

namespace BuilderHomeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var constructorList = new List<Constructor>();
            constructorList.Add(new Constructor("Vasya", 2, 2, 3, 2, 4, 4, 2, 2));
            constructorList.Add(new Constructor("Sasha", 4, 4, 1, 4, 2, 2, 3, 3));

            var actionList = new List<string>();
            actionList.Add(nameof(HomeBuilder.BuildFoundation));
            actionList.Add(nameof(HomeBuilder.BuildWall));
            actionList.Add(nameof(HomeBuilder.BuildDoor));
            actionList.Add(nameof(HomeBuilder.BuildRoof));
            actionList.Add(nameof(HomeBuilder.BuildWindow));
            actionList.Add(nameof(HomeBuilder.BuildFloor));
            actionList.Add(nameof(HomeBuilder.BuildWallPaper));
            var director = new Director(constructorList, actionList);
            director.Build();

            director.GetResult ();
            Console.ReadLine();
        }
    }
}
