﻿using BuilderHomeApp.Entities;

namespace BuilderHomeApp
{
    public class Home
    {
        public Wall Wall { get; set; }
        public Roof Roof { get; set; }
        public Foundation Foundation { get; set; }
        public Door Door { get; set; }
        public Window Window { get; set; }
        public Painting Painting { get; set; }
        public Floor Floor { get; set; }
        public WallPaper WallPaper { get; set; }
    }
}
