﻿namespace BuilderHomeApp
{
    public class Constructor
    {
        public string Name { get; set; }
        public int TimeDoor { get; set; }
        public int TimeFloor { get; set; }
        public int TimeFoundation { get; set; }
        public int TimePainting { get; set; }
        public int TimeRoof { get; set; }
        public int TimeWall{ get; set; }
        public int TimeWallPaper { get; set; }
        public int TimeWindow { get; set; }

        public Constructor(string name, int timeDoor, int timeFloor, int timeFoundation, int timePainting, int timeRoof, int timeWall, int timeWallPaper, int timeWindow)
        {
            Name = name;
            TimeDoor = timeDoor;
            TimeFloor = timeFloor;
            TimeFoundation = timeFoundation;
            TimePainting = timePainting;
            TimeRoof = timeRoof;
            TimeWall = timeWall;
            TimeWallPaper = timeWallPaper;
            TimeWindow = timeWindow;
        }
    }
}
