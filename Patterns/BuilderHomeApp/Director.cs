﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BuilderHomeApp
{
    public class Director
    {
        private HomeBuilder _homeBuilder = new HomeBuilder();
        private List<Constructor> _constructors = new List<Constructor>();
        private List<Action> _actions = new List<Action>();
        private int _constructionTime = 0;

        public Director(List<Constructor> constructorList, List<string> actionList)
        {
            SetConstructor(constructorList);
            SetAction(actionList);
        }

        public void Build()
        {
            int i = 0;
            while (_actions.Count > 0)
            {
                var action = _actions[i]; 
                var constructor = SelectConstructor(action.Method.Name, out int time);
                try
                {
                    action();
                    Console.WriteLine($"Constructor: {constructor.Name} {action.Method.Name}");
                    _constructionTime += time;
                    _actions.Remove(action);
                    i--;
                }
                catch (ApplicationException e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    i++;
                    if (i >= _actions.Count)
                    {
                        i = 0;
                    }
                }
            }

        }

        private Constructor SelectConstructor(string actionName, out int time)
        {
            time = 0;
            Constructor constructor;
            switch (actionName)
            {
                case nameof(HomeBuilder.BuildFoundation):
                    constructor = _constructors.First(x => x.TimeFoundation == _constructors.Min(z => z.TimeFoundation));
                    time = constructor.TimeFoundation;
                    return constructor;
                case nameof(HomeBuilder.BuildWall):
                    constructor = _constructors.First(x => x.TimeWall == _constructors.Min(z => z.TimeWall));
                    time = constructor.TimeWall;
                    return constructor;
                case nameof(HomeBuilder.BuildDoor):
                    constructor = _constructors.First(x => x.TimeDoor == _constructors.Min(z => z.TimeDoor));
                    time = constructor.TimeDoor;
                    return constructor;
                case nameof(HomeBuilder.BuildRoof):
                    constructor = _constructors.First(x => x.TimeRoof== _constructors.Min(z => z.TimeRoof));
                    time = constructor.TimeRoof;
                    return constructor;
                case nameof(HomeBuilder.BuildWindow):
                    constructor = _constructors.First(x => x.TimeWindow == _constructors.Min(z => z.TimeWindow));
                    time = constructor.TimeWindow;
                    return constructor;
                case nameof(HomeBuilder.BuildFloor):
                    constructor = _constructors.First(x => x.TimeFloor == _constructors.Min(z => z.TimeFloor));
                    time = constructor.TimeFloor;
                    return constructor;
                case nameof(HomeBuilder.BuildPainting):
                    constructor = _constructors.First(x => x.TimePainting == _constructors.Min(z => z.TimePainting));
                    time = constructor.TimePainting;
                    return constructor;
                case nameof(HomeBuilder.BuildWallPaper):
                    constructor = _constructors.First(x => x.TimeWallPaper == _constructors.Min(z => z.TimeWallPaper));
                    time = constructor.TimeWallPaper;
                    return constructor;
            }

            return null;
        }

        public void SetConstructor(List<Constructor> constructorList)
        {
            _constructors = constructorList;
        }
        public void SetAction(List<string> actionList)
        {
            _actions = new List<Action>();
            foreach (var action in actionList)
            {
                switch (action)
                {
                    case nameof(HomeBuilder.BuildFoundation):
                        _actions.Add(_homeBuilder.BuildFoundation);
                        break;
                    case nameof(HomeBuilder.BuildRoof):
                        _actions.Add(_homeBuilder.BuildRoof);
                        break;
                    case nameof(HomeBuilder.BuildDoor):
                        _actions.Add(_homeBuilder.BuildDoor);
                        break;
                    case nameof(HomeBuilder.BuildFloor):
                        _actions.Add(_homeBuilder.BuildFloor);
                        break;
                    case nameof(HomeBuilder.BuildPainting):
                        _actions.Add(_homeBuilder.BuildPainting);
                        break;
                    case nameof(HomeBuilder.BuildWall):
                        _actions.Add(_homeBuilder.BuildWall);
                        break;
                    case nameof(HomeBuilder.BuildWallPaper):
                        _actions.Add(_homeBuilder.BuildWallPaper);
                        break;
                    case nameof(HomeBuilder.BuildWindow):
                        _actions.Add(_homeBuilder.BuildWindow);
                        break;
                }
            }
        }

        public Home GetResult ()
        {
            try
            {
                var home = _homeBuilder.GetResult ();
                Console.WriteLine($"Construction time: {_constructionTime}");
                return home;
            }
            catch (ApplicationException e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        public void ResetHome()
        {
            _homeBuilder = new HomeBuilder();
            _constructionTime = 0;
        }
    }
}
