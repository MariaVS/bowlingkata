﻿namespace BuilderHomeApp
{
    public interface IBuilder
    {
        Home GetResult ();
        void BuildDoor();
        void BuildFoundation();
        void BuildRoof();
        void BuildWall();
        void BuildWindow();
        void BuildWallPaper();
        void BuildFloor();
        void BuildPainting();
    }
}
