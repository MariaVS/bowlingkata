﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BoardApp.DAL.Model
{
    public class Board
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(30)]
        public string Title { get; set; }
        [MaxLength(200)]
        public string Description { get; set; }
        public virtual ICollection<BoardAccess> BoardAccesses { get; set; }
        public virtual ICollection<Column> Columns { get; set; }
    }
}
