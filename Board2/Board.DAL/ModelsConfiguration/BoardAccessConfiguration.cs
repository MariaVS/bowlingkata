﻿using BoardApp.DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BoardApp.DAL.ModelsConfiguration
{
    public class BoardAccessConfiguration : IEntityTypeConfiguration<BoardAccess>
    {
        public void Configure(EntityTypeBuilder<BoardAccess> builder)
        {
            builder.HasIndex(c => new { c.UserId, c.BoardId }).IsClustered();
            builder.HasData(
                new BoardAccess { BoardId = 1, UserId = 1, PermissionId = 1},
                new BoardAccess { BoardId = 2, UserId = 2, PermissionId = 2});

        }
    }
}
