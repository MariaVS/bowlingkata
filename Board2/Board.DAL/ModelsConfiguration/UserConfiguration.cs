﻿using BoardApp.DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BoardApp.DAL.ModelsConfiguration
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasAlternateKey(c => c.Email);
            builder.HasAlternateKey(c => c.Login);
            builder.HasData(
                new User { Id = 1, Email = "ArtemVT@gmail.com", Firstname = "Artem", Lastname = "Valtert", Login = "ArtemVT", Password = "ArtemVT29018"},
                new User { Id = 2, Email = "DimaKR", Firstname = "Dima", Lastname = "Karabanovich", Login = "DimaKR", Password = "DimaKR09267"});
        }
    }
}
