﻿using System.Collections.Generic;

namespace BoardApp.DAL.Model
{
    public class Card
    {
        public int Id { get; set; }
        public int ColumnId { get; set; }
        public virtual Column Column { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual HashSet<Label> Labels { get; set; }
        public virtual HashSet<Comment> Comments { get; set; }
        public virtual HashSet<User> Users { get; set; }
    }
}
