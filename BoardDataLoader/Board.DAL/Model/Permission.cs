﻿using System.Collections.Generic;

namespace BoardApp.DAL.Model
{
    public class Permission
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual HashSet<BoardAccess> BoardAccesses { get; set; }
    }
}
