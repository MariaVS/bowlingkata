﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace BoardApp.Common
{
    public static class XmlSerializer<T>
    {
        public static void Serialize(T instance, string fileRoute, Type objectType)
        {
            var formatter = new XmlSerializer(typeof(T));
            using var fs = new FileStream(fileRoute, FileMode.Open);
            formatter.Serialize(fs, instance);
        }

        public static T Deserialize(string fileRoute, Type objectType)
        {
            var formatter = new XmlSerializer(typeof(T));
            using var fs = new FileStream(fileRoute, FileMode.Open);
            return (T)formatter.Deserialize(fs);
        }
    }
}
