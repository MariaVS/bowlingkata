using System;
using System.Diagnostics;
using System.IO;
using BoardApp.DAL;
using BoardDataLoader.QuartzBoard;
using BoardDataLoader.Xml;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;

namespace BoardDataLoader
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    //logging.AddEventLog();
                    logging.AddDebug();
                })
                .UseWindowsService(options =>
                {
                    options.ServiceName = "Board Data Loader Service";
                })
                .ConfigureAppConfiguration((hostContext, builder) =>
                {
                    builder.SetBasePath(AppDomain.CurrentDomain.BaseDirectory);
                    builder.AddJsonFile("appsettings.json");
                })
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;
                    var connectionString = configuration.GetConnectionString("DefaultConnection");
                    services.AddDbContext<BoardDbContext>(builder =>
                    {
                        builder.LogTo(message => Debug.WriteLine(message));
                        builder.UseLazyLoadingProxies().UseSqlServer(connectionString);
                    }, ServiceLifetime.Singleton);

                    services.AddSingleton(new FileSystemWatcher(configuration.GetValue<string>("WatchedUsersFolderPath")));
                    services.AddTransient<IXmlService, XmlService>();
                    services.AddTransient<IFileLogger, FileLogger>();
                    services.AddHostedService<Worker>();
                    
                    services.AddQuartz(config =>
                    {
                        config.UseMicrosoftDependencyInjectionJobFactory();

                        var jobKey = new JobKey("BoardJob");
                        config.AddJob<BoardJob>(opts => opts.WithIdentity(jobKey));
                        config.AddTrigger(opts => opts
                            .ForJob(jobKey)
                            .WithIdentity("BoardJob-trigger")
                            .WithCronSchedule(configuration.GetValue<string>("CronExpression")));
                    });

                    services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);
                });
    }
}
