﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BoardDataLoader
{
    public class FileLogger : IFileLogger
    {

        public void LogToFile(string fileRoute, Exception ex)
        {
            if (!File.Exists(fileRoute))
            {
                using var stream = File.Create(fileRoute);
                Write(stream);
            }
            else
            {
                using var stream = File.Open(fileRoute, FileMode.Append);
                Write(stream);
            }

            void Write(FileStream stream)
            {
                using var sw = new StreamWriter(stream);
                LogInnerException(sw, ex);
            }
        }

        public void LogDuplicationsToFile(string fileRoute, List<string> errors)
        {
            if (!File.Exists(fileRoute))
            {
                using var stream = File.Create(fileRoute);
                Write(stream);
            }
            else
            {
                using var stream = File.Open(fileRoute, FileMode.Append);
                Write(stream);
            }

            void Write(FileStream stream)
            {
                using var sw = new StreamWriter(stream);
                foreach (var error in errors)
                {
                    sw.WriteLine(error);
                }
            }
        }

        private static void LogInnerException(StreamWriter sw, Exception ex)
        {
            if (ex is not null)
            {
                sw.WriteLine(ex.Message);
                sw.WriteLine(ex.StackTrace);
                LogInnerException(sw, ex.InnerException);
            }
        }
    }
}
