﻿using System;
using System.Collections.Generic;

namespace BoardDataLoader
{
    public interface IFileLogger
    {
        void LogToFile(string fileRoute, Exception ex);
        void LogDuplicationsToFile(string fileRoute, List<string> errors);
    }
}
