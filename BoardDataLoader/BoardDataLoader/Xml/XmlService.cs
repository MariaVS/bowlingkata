﻿using System;
using System.Collections.Generic;
using BoardApp.Common;
using Microsoft.Extensions.Logging;

namespace BoardDataLoader.Xml
{
    public class XmlService : IXmlService
    {
        private readonly ILogger<XmlService> _logger;

        public XmlService(ILogger<XmlService> logger)
        {
            _logger = logger;
        }

        public IList<T> ReadAll<T>(string fileRoute)
        {
            return XmlSerializer<List<T>>.Deserialize(fileRoute, typeof(T));
        }
    }
}
