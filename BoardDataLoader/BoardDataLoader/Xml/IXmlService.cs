﻿using System.Collections.Generic;

namespace BoardDataLoader.Xml
{
    public interface IXmlService
    {
        IList<T> ReadAll<T>(string fileRoute);
    }
}
