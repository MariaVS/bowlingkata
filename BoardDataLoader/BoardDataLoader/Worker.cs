using BoardApp.DAL;
using BoardApp.DAL.Model;
using BoardDataLoader.Xml;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BoardDataLoader
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly BoardDbContext _ctx;
        private readonly FileSystemWatcher _watcher;
        private readonly IXmlService _xmlService;
        private readonly IFileLogger _fileLogger;

        public Worker(ILogger<Worker> logger, BoardDbContext ctx, FileSystemWatcher watcher, IXmlService xmlService, IFileLogger fileLogger)
        {
            _logger = logger;
            _ctx = ctx;
            _watcher = watcher;
            _xmlService = xmlService;
            _fileLogger = fileLogger;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
            InitializeWatcher();
            _logger.LogInformation("File watcher initialized");
            return Task.CompletedTask;
        }

        private void InitializeWatcher()
        {
            _watcher.NotifyFilter = NotifyFilters.FileName;

            _watcher.Created += OnCreated;

            _watcher.Filter = "*.xml";
            _watcher.IncludeSubdirectories = true;
            _watcher.EnableRaisingEvents = true;
        }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            try
            {
                string value = $"Created: {e.FullPath}";
                _logger.LogInformation(value);
                var users = _xmlService.ReadAll<User>(e.FullPath);

                var duplicatedErrors = new List<string>();
                var usersToAdd = new List<User>();
                var existingUsers = _ctx.Users.ToList();
                foreach (var user in users)
                {
                    var duplicatedUser = false;
                    foreach (var existingUser in existingUsers)
                    {
                        var error = string.Empty;
                        if (existingUser.FirstName == user.FirstName && existingUser.LastName == user.LastName)
                        {
                            error += $"User with duplicated name {existingUser.FirstName} {existingUser.LastName} can not be added.\n";
                            duplicatedUser = true;
                        }
                        if (existingUser.Login == user.Login)
                        {
                            error += $"User with duplicated login {existingUser.Login} can not be added.\n";
                            duplicatedUser = true;
                        }
                        if (existingUser.Email == user.Email)
                        {
                            error += $"User with duplicated email {existingUser.Email} can not be added.\n";
                            duplicatedUser = true;
                        }

                        if (error != string.Empty)
                        {
                            error += $"Database id: {existingUser.Id}";
                            duplicatedErrors.Add(error);
                        }
                    }
                    if (!duplicatedUser)
                    {
                        usersToAdd.Add(user);
                    }
                }

                _ctx.Users.AddRange(usersToAdd);
                _ctx.SaveChanges();
                if (duplicatedErrors.Any())
                {
                    var index = e.FullPath.LastIndexOf(".", StringComparison.InvariantCulture);
                    var path = e.FullPath.Substring(0, index) + "_Duplicated.log";
                    _fileLogger.LogDuplicationsToFile(path, duplicatedErrors);
                    _logger.LogInformation($"Data from {e.Name} was loaded, encountered duplications");
                }
                else
                {
                    _logger.LogInformation($"Data from {e.Name} was loaded successfully");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                var index = e.FullPath.LastIndexOf(".", StringComparison.InvariantCulture);
                var path = e.FullPath.Substring(0, index) + ".log";
                _fileLogger.LogToFile(path, ex);
            }
        }
    }
}
