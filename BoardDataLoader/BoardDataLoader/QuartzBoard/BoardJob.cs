﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BoardApp.DAL;
using BoardApp.DAL.Model;
using BoardDataLoader.Xml;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;

namespace BoardDataLoader.QuartzBoard
{
    public class BoardJob : IJob
    {
        private readonly IConfiguration _configuration;
        private readonly IXmlService _xmlService;
        private readonly Random _random;
        private readonly ILogger<Worker> _logger;
        private readonly IFileLogger _fileLogger;
        private readonly BoardDbContext _ctx;

        public BoardJob(IConfiguration configuration, IXmlService xmlService, ILogger<Worker> logger, BoardDbContext ctx, IFileLogger fileLogger)
        {
            _configuration = configuration;
            _xmlService = xmlService;
            _random = new Random();
            _logger = logger;
            _ctx = ctx;
            _fileLogger = fileLogger;
        }

        public Task Execute(IJobExecutionContext context)
        {
            var folderPath = _configuration.GetValue<string>("WatchedBoardsFolderPath");
            var filePaths = Directory.GetFiles(folderPath, "*.xml").ToList();

            foreach (var filePath in filePaths)
            {
                try
                {
                    var boards = _xmlService.ReadAll<Board>(filePath);
                    var duplicatedErrors = new List<string>();
                    var boardsToAdd = new List<Board>();
                    var existingBoards = _ctx.Boards.ToList();
                    foreach (var board in boards)
                    {
                        var duplicatedBoard = false;
                        foreach (var existingBoard in existingBoards)
                        {
                            var error = string.Empty;
                            if (existingBoard.Title == board.Title)
                            {
                                error += $"Board with duplicated title {existingBoard.Title} can not be added.\n";
                                duplicatedBoard = true;
                            }
                            if (error != string.Empty)
                            {
                                error += $"Database id: {existingBoard.Id}";
                                duplicatedErrors.Add(error);
                            }
                        }
                        if (!duplicatedBoard)
                        {
                            boardsToAdd.Add(board);
                        }
                    }

                    AssignBoardsToUsers(boardsToAdd);
                    _ctx.Boards.AddRange(boardsToAdd);
                    _ctx.SaveChanges();
                    var indexForLogInf = filePath.LastIndexOf(@"\", StringComparison.InvariantCulture);
                    var pathForLogInf = filePath.Substring(indexForLogInf);
                    if (duplicatedErrors.Any())
                    {
                        var index = filePath.LastIndexOf(".", StringComparison.InvariantCulture);
                        var path = filePath.Substring(0, index) + "_Duplicated.log";
                        _fileLogger.LogDuplicationsToFile(path, duplicatedErrors);
                        _logger.LogInformation($"Data from {pathForLogInf} was loaded, encountered duplications");
                    }
                    else
                    {
                        _logger.LogInformation($"Data from {pathForLogInf} was loaded successfully");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    var index = filePath.LastIndexOf(".", StringComparison.InvariantCulture);
                    var path = filePath.Substring(0, index) + ".log";
                    _fileLogger.LogToFile(path, ex);
                }
            }
            return Task.CompletedTask;
        }

        private void AssignBoardsToUsers(List<Board> boards)
        {
            if (!boards.Any())
            {
                return;
            }
            var users = _ctx.Users.ToList();
            var permissions = _ctx.Permissions.ToList();
            foreach (var board in boards)
            {
                var userIndex = _random.Next(users.Count);
                var permissionIndex = _random.Next(permissions.Count);

                var boardAccess = new BoardAccess { Board = board, Permission = permissions[permissionIndex], User = users[userIndex] };
                _ctx.BoardAccesses.Add(boardAccess);
            }
        }
    }
}
