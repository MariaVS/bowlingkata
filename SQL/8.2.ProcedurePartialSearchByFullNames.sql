USE Internship
GO

CREATE OR ALTER PROCEDURE uds.PartialSearchByFullNames(
	@FirstName nvarchar(50),
	@LastName nvarchar(50))
AS 
BEGIN
SELECT Id, FirstName, LastName, Age
FROM uds.Users u
WHERE u.FirstName like '%'+@FirstName+'%' AND u.LastName like '%'+@LastName+'%'
END
GO