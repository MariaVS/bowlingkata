USE Internship
GO

/*
INSERT INTO uds.ExecutionStatistics
VALUES ('AddUser', 'Table Insert', 0, '00:00:00', 5.3, 1, 0.01, 0.010001, 0.000001, 0.0100023)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddUser', 'Compute Scalar', 0, '00:00:00', 5.3, 1, 0, 0.0000001, 0.0000001, 0.0000013)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddUser', 'Constant Scan', 0, '00:00:00', 5.3, 1, 0, 0.0000012, 0.0000012, 0.0000012)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddPaymentForUser', 'Table Insert', 0, '00:00:00', 8.7, 1, 0.01, 0.010001, 0.000001, 0.0100023)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddPaymentForUser', 'Compute Scalar', 0, '00:00:00', 8.7, 1, 0, 0.0000001, 0.0000001, 0.0000013)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddPaymentForUser', 'Constant Scan', 0, '00:00:00', 8.7, 1, 0, 0.0000012, 0.0000012, 0.0000012)
INSERT INTO uds.ExecutionStatistics
VALUES ('GetUserById', 'Table Scan', 0, '00:00:00.075', 4.4, 1000002, 4.51794, 5.6181, 1.10016, 5.6181)
INSERT INTO uds.ExecutionStatistics
VALUES ('GetPaymentsByUserId', 'Table Scan', 0, '00:00:00.401', 11.5, 10000005, 20.1861, 31.1863, 11.0002, 31.1863)
INSERT INTO uds.ExecutionStatistics
VALUES ('GetPaymentsById', 'Table Scan', 0, '00:00:00.391', 6.5, 10000005, 20.1861, 31.1863, 11.0002, 31.1863)
*/

/*
INSERT INTO uds.ExecutionStatistics
VALUES ('AddUser', 'Clustered Index Insert', 1, '00:00:00', 2.1, 1, 0.01, 0.010001, 0.000001, 0.0100023)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddUser', 'Compute Scalar', 1, '00:00:00', 5.3, 1, 0, 0.0000001, 0.0000001, 0.0000013)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddUser', 'Constant Scan', 1, '00:00:00', 5.3, 1, 0, 0.0000012, 0.0000012, 0.0000012)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddPaymentForUser', 'Assert', 1, '00:00:00', 4.4, 1, 0, 0.0000002, 0.0000002, 0.0232907)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddPaymentForUser', 'Nested Loops', 1, '00:00:00', 4.4, 1, 0, 0.0000041, 0.0000042, 0.0232905)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddPaymentForUser', 'Clustered Index Insert', 1, '00:00:00', 4.4, 1, 0.02, 0.020002, 0.000002, 0.0200033)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddPaymentForUser', 'Clustered Index Seek', 1, '00:00:00', 4.4, 1, 0.003125, 0.0032831, 0.0001581, 0.0032831)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddPaymentForUser', 'Compute Scalar', 1, '00:00:00', 4.4, 1, 0, 0.0000001, 0.0000001, 0.0000013)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddPaymentForUser', 'Constant Scan', 1, '00:00:00', 4.4, 1, 0, 0.0000012, 0.0000012, 0.0000012)
INSERT INTO uds.ExecutionStatistics
VALUES ('GetUserById', 'Clustered Index Seek', 1, '00:00:00', 1.1, 1, 0.003125, 0.0032831, 0.0001581, 0.0032831)
INSERT INTO uds.ExecutionStatistics
VALUES ('GetPaymentsByUserId', 'Nested Loops', 1, '00:00:00', 3.8, 5, 0, 0.0000209, 0.0000209, 0.0165982)
INSERT INTO uds.ExecutionStatistics
VALUES ('GetPaymentsByUserId', 'Index Seek (NonClustered)', 1, '00:00:00', 3.8, 5, 0.003125, 0.0032875, 0.0001625, 0.0032875)
INSERT INTO uds.ExecutionStatistics
VALUES ('GetPaymentsByUserId', 'Key Lookup (Clustered)', 1, '00:00:00', 3.8, 5, 0.003125, 0.0132898, 0.0001581, 0.0132898)
INSERT INTO uds.ExecutionStatistics
VALUES ('GetPaymentsById', 'Clustered Index Seek', 1, '00:00:00.391', 2.2, 1, 0.003125, 0.0032831, 0.0001581, 0.0032831)
*/

/*
INSERT INTO uds.ExecutionStatistics
VALUES ('FullSearchByFullNames', 'Clustered Index Scan', 0, '00:00:00.086', 7.9, 1000004, 4.31794, 5.4181, 1.10016, 5.4181)
INSERT INTO uds.ExecutionStatistics
VALUES ('PartialSearchByFullNames', 'Clustered Index Scan', 0, '00:00:00.777', 13.6, 1000004, 4.31794, 5.4181, 1.10016, 5.4181)
INSERT INTO uds.ExecutionStatistics
VALUES ('FullSearchByFullNames', 'Index Seek (NonClustered)', 1, '00:00:00', 3.1, 32, 0.003125, 0.0033193, 0.0001943, 0.0033193)
INSERT INTO uds.ExecutionStatistics
VALUES ('PartialSearchByFullNames', 'Nested Loops', 1, '00:00:00.774', 12.4, 191, 0.0646065, 0, 0.0169983, 0.0816048)
INSERT INTO uds.ExecutionStatistics
VALUES ('PartialSearchByFullNames', 'Compute Scalar', 1, '00:00:00', 12.4, 0, 0, 0, 0, 0)
INSERT INTO uds.ExecutionStatistics
VALUES ('PartialSearchByFullNames', 'Constant Scan', 1, '00:00:00', 12.4, 0, 0, 0, 0, 0)
INSERT INTO uds.ExecutionStatistics
VALUES ('PartialSearchByFullNames', 'Index Seek (NonClustered)', 1, '00:00:00.774', 12.4, 1000004, 0.0646065, 0.0816048, 0.0169983, 0.0816048)
*/

/*
INSERT INTO uds.ExecutionStatistics
VALUES ('AddUser', 'Clustered Index Insert', 1, '00:00:00.009', 4.2, 1, 0.02, 0.020002, 0.000002, 0.0200033)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddUser', 'Compute Scalar', 1, '00:00:00', 4.2, 1, 0, 0.0000001, 0.0000001, 0.0000013)
INSERT INTO uds.ExecutionStatistics
VALUES ('AddUser', 'Constant Scan', 1, '00:00:00', 4.2, 1, 0, 0.0000012, 0.0000012, 0.0000012)
*/
/*
ActionName nvarchar(50),
	OperatorName nvarchar(50),
	Indexed bit,
	ExecutionTime time,
	CpuUsage decimal(5,2),
	ActualNumberOfRows int,
	EstimatedIoCost decimal(10,7),
	EstimatedOperatorCost decimal(10,7),
	EstimatedCpuCost decimal(10,7),
	EstimatedSubtreeCost decimal(10,7)
*/
/*
INSERT INTO uds.ExecutionStatistics
VALUES ('FTS', 'Merge Join', 1, '00:00:00.186', 5.5, 33, 0, 2.1164942, 2.11649, 7.54558)
INSERT INTO uds.ExecutionStatistics
VALUES ('FTS', 'FullTextMatch (Function)', 1, '00:00:00', 5.5, 33, 0.008692, 0.0109858, 0.0022938, 0.0109858)
INSERT INTO uds.ExecutionStatistics
VALUES ('FTS', 'Clustered Index scan', 1, '00:00:00.139', 5.5, 1000005, 4.31794, 5.4181, 1.10016, 5.4181)
*/

/*
INSERT INTO uds.ExecutionStatistics
VALUES ('FTS DoubleSurname', 'Merge Join', 1, '00:00:00.00', 3.0, 13318, 0, 2.1349207, 2.13491, 7.57148)
INSERT INTO uds.ExecutionStatistics
VALUES ('FTS DoubleSurname', 'FullTextMatch (Function)', 1, '00:00:00', 3.0, 13318, 0.0056325, 0.0184593, 0.0056325, 0.0184593)
INSERT INTO uds.ExecutionStatistics
VALUES ('FTS DoubleSurname', 'Clustered Index scan', 1, '00:00:00.00', 3.0, 1000010, 4.31794, 5.4181, 1.10016, 5.4181)
*/