USE Internship
GO

CREATE NONCLUSTERED INDEX IX_Users_FirstName_LastName
ON uds.Users (FirstName, LastName)
INCLUDE (Id, Age)