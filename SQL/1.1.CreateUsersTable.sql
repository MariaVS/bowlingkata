USE Internship
GO

CREATE TABLE uds.Users (
	Id int identity(1,1) NOT NULL,
	Age int NOT NULL,
	FirstName nvarchar(50) NOT NULL,
	LastName nvarchar(50) NOT NULL
)