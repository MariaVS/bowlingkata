USE Internship
GO

CREATE NONCLUSTERED INDEX IX_Payments_UserId
ON uds.Payments (UserId ASC)