USE Internship
GO

CREATE OR ALTER PROCEDURE uds.SeedDB 
AS 
BEGIN
DECLARE @Counter INT,
@Counter2 INT,
@UserId int,
@FirstName nvarchar(50),
@LastName nvarchar(50)
SET @Counter=1
WHILE ( @Counter <= 1000000)
BEGIN
	PRINT @Counter
    --Create User
	SELECT TOP 1 @FirstName = FirstName FROM uds.FirstNames
	ORDER BY NEWID()
	SELECT TOP 1 @LastName = LastName FROM uds.LastNames
	ORDER BY NEWID()
	INSERT INTO uds.Users(FirstName, LastName, Age)
	VALUES (@FirstName, @LastName, RAND()*90 + 1)
	SET @UserId = SCOPE_IDENTITY()
	--Create Payments
	SET @Counter2=1
	WHILE ( @Counter2 <= 10)
	BEGIN
		INSERT INTO uds.Payments(Price, UserId)
		VALUES(RAND()*100000 + 1, @UserId)
		SET @Counter2  = @Counter2 + 1
	END
    SET @Counter  = @Counter + 1
END
END
GO