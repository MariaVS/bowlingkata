USE Internship
GO

CREATE TABLE uds.ExecutionStatistics (
	Id int identity(1,1) Primary key NOT NULL,
	ActionName nvarchar(50),
	OperatorName nvarchar(50),
	Indexed bit,
	ExecutionTime time,
	CpuUsage decimal(5,2),
	ActualNumberOfRows int,
	EstimatedIoCost decimal(10,7),
	EstimatedOperatorCost decimal(10,7),
	EstimatedCpuCost decimal(10,7),
	EstimatedSubtreeCost decimal(10,7)
)