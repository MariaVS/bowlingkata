USE Internship
GO

ALTER TABLE uds.Payments
ADD CONSTRAINT FK_Payments_Users FOREIGN KEY (UserId)
        REFERENCES uds.Users (Id)
        ON DELETE CASCADE
        ON UPDATE CASCADE