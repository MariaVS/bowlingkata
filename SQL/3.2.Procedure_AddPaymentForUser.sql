USE Internship
GO

CREATE OR ALTER PROCEDURE uds.AddPaymentForUser( @Price int, @UserId int)
AS 
BEGIN
INSERT INTO uds.Payments(Price, UserId)
VALUES(@Price, @UserId)
END
GO