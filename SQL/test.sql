--EXEC uds.SeedDB

/*
EXEC uds.AddUser 'Aaron', 'Powell', 35
EXEC uds.AddUser 'Alexander', 'Powell-Martincave', 35
EXEC uds.AddPaymentForUser 20345, 1000003
EXEC uds.GetUserById 1000001
EXEC uds.GetPaymentsByUserId 1000001
EXEC uds.GetPaymentsById 10000001

EXEC uds.FullSearchByFullNames 'Aaron', 'Powell'
EXEC uds.PartialSearchByFullNames 'aro', 'owe'

select count(*) from uds.Users
select count(*) from uds.Payments
*/


/*
CONSTRAINT FK_Payments_Users FOREIGN KEY (UserId)
        REFERENCES dbo.Users (Id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
*/

select * from uds.Users
where CONTAINS(FirstName, '"Al*"') and CONTAINS(LastName, '"Mar*"')