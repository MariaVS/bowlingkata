USE Internship
GO

SELECT 
	CASE FULLTEXTSERVICEPROPERTY('IsFullTextInstalled')
		WHEN 1 THEN 'Full-Text installed.' 
		ELSE 'Full-Text is NOT installed.' 
	END
;

SELECT is_fulltext_enabled
FROM sys.databases
WHERE database_id = DB_ID()

exec sp_fulltext_database 'enable'; 

select * FROM sys.fulltext_catalogs

--CREATE FULLTEXT CATALOG FullTextCatalog AS DEFAULT;

sp_help 'uds.Users'

select * from sys.fulltext_indexes
select * from sys.fulltext_index_fragments
SELECT * FROM sys.fulltext_languages  
ORDER BY lcid

select * from sys.dm_fts_active_catalogs
select * from sys.dm_fts_fdhosts
select * from sys.dm_fts_index_population
select * from sys.dm_fts_population_ranges

ALTER FULLTEXT INDEX ON uds.Users START UPDATE POPULATION;  
GO 