USE Internship
GO

CREATE OR ALTER PROCEDURE uds.GetPaymentsByUserId(@UserId int)
AS 
BEGIN
SELECT Id, Price, UserId
FROM uds.Payments p
WHERE p.UserId = @UserId 
END
GO