USE Internship
GO

CREATE OR ALTER PROCEDURE uds.GetPaymentsById(@Id int)
AS 
BEGIN
SELECT Id, Price, UserId
FROM uds.Payments p
WHERE p.Id = @Id 
END
GO