USE Internship
GO

CREATE OR ALTER PROCEDURE uds.AddUser( @FirstName nvarchar(50), @LastName nvarchar(50), @Age int)
AS 
BEGIN
INSERT INTO uds.Users(FirstName, LastName, Age)
VALUES(@FirstName, @LastName, @Age)
END
GO