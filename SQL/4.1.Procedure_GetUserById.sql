USE Internship
GO

CREATE OR ALTER PROCEDURE uds.GetUserById(@Id int)
AS 
BEGIN
SELECT Id, FirstName, LastName, Age
FROM uds.Users u
WHERE u.Id = @Id
END
GO