USE Internship
GO

CREATE OR ALTER PROCEDURE uds.FullSearchByFullNames(
	@FirstName nvarchar(50),
	@LastName nvarchar(50))
AS 
BEGIN
SELECT Id, FirstName, LastName, Age
FROM uds.Users u
WHERE u.FirstName = @FirstName AND u.LastName = @LastName
END
GO