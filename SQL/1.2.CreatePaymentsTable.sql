USE Internship
GO

CREATE TABLE uds.Payments (
	Id int identity(1,1) NOT NULL,
	Price int NOT NULL,
	UserId int NOT NULL
)