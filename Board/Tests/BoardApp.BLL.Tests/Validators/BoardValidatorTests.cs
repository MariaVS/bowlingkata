﻿using BoardApp.BLL.Tests.Helpers;
using BoardApp.BLL.Validators;
using BoardApp.Common.Models;
using FluentValidation;
using Xunit;

namespace BoardApp.BLL.Tests.Validators
{
    public class BoardValidatorTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenTitleIsNullOrEmpty_ThenThrow(string title)
        {
            //Arrange
            var board = new Board {Title = title};
            var validator = new BoardValidator();
            var error = ValidationHelper.WrapError(nameof(Board.Title), $"'{nameof(Board.Title)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(board));
            Assert.Equal(error, ex.Message);
        }


        [Fact]
        public void Validate_WhenModelIsRight_ThenPass()
        {
            //Arrange
            var board = new Board { Title = "new" };
            var validator = new BoardValidator();

            //Act
            validator.ValidateAndThrow(board);

            //Assert
            Assert.True(true);
        }
    }
}
