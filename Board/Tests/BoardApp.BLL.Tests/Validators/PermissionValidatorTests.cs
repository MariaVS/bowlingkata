﻿using BoardApp.BLL.Tests.Helpers;
using BoardApp.BLL.Validators;
using BoardApp.Common.Models;
using FluentValidation;
using Xunit;


namespace BoardApp.BLL.Tests.Validators
{
    public class PermissionValidatorTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenNameIsNullOrEmpty_ThenThrow(string name)
        {
            //Arrange
            var permission = new Permission { Name = name };
            var validator = new PermissionValidator();
            var error = ValidationHelper.WrapError(nameof(Permission.Name), $"'{nameof(Permission.Name)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(permission));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenNameMoreThan30_ThenThrow()
        {
            //Arrange
            var name = new string('1', 31);
            var permission = new Permission { Name = name };
            var validator = new PermissionValidator();
            var error = ValidationHelper.WrapError(nameof(Permission.Name), $"The length of '{nameof(Permission.Name)}' must be 30 characters or fewer. You entered 31 characters.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(permission));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenModelIsRight_ThenPass()
        {
            //Arrange
            var permission = new Permission { Name = "new" };
            var validator = new PermissionValidator();

            //Act
            validator.ValidateAndThrow(permission);

            //Assert
            Assert.True(true);
        }
    }
}
