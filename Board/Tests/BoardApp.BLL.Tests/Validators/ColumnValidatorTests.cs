﻿using BoardApp.BLL.Tests.Helpers;
using BoardApp.BLL.Validators;
using BoardApp.Common.Models;
using FluentValidation;
using Xunit;

namespace BoardApp.BLL.Tests.Validators
{
    public class ColumnValidatorTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenTitleIsNullOrEmpty_ThenThrow(string title)
        {
            //Arrange
            var column = new Column { Title = title };
            var validator = new ColumnValidator();
            var error = ValidationHelper.WrapError(nameof(Column.Title), $"'{nameof(Column.Title)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(column));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenModelIsRight_ThenPass()
        {
            //Arrange
            var column = new Column { Title = "new" };
            var validator = new ColumnValidator();

            //Act
            validator.ValidateAndThrow(column);

            //Assert
            Assert.True(true);
        }
    }
}
