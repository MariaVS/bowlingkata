﻿using BoardApp.BLL.Tests.Helpers;
using BoardApp.BLL.Validators;
using BoardApp.Common.Models;
using FluentValidation;
using Xunit;

namespace BoardApp.BLL.Tests.Validators
{
    public class UserValidatorTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenFirstNameIsNullOrEmpty_ThenThrow(string name)
        {
            //Arrange
            var user = new User { FirstName = name, LastName = "Tremonin", Login = "VTr109", Email = "VTr@gmail.com", Password = "VTr9087" };
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.FirstName), $"'{nameof(User.FirstName)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenLastNameIsNullOrEmpty_ThenThrow(string name)
        {
            //Arrange
            var user = new User { FirstName = "Vli", LastName = name, Login = "VTr109", Email = "VTr@gmail.com", Password = "VTr9087" };
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.LastName), $"'{nameof(User.LastName)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenEmailIsNullOrEmpty_ThenThrow(string email)
        {
            //Arrange
            var user = new User { FirstName = "Vli", LastName = "Vs", Email = email, Login = "hyt", Password = "VTr9087" };
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.Email), $"'{nameof(User.Email)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenLoginIsNullOrEmpty_ThenThrow(string login)
        {
            //Arrange
            var user = new User { FirstName = "Vli", LastName = "Vs", Email = "kjhhf", Login = login, Password = "kjhf123"};
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.Login), $"'{nameof(User.Login)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenPasswordIsNullOrEmpty_ThenThrow(string password)
        {
            //Arrange
            var user = new User { FirstName = "Vli", LastName = "Vs", Email = "kjhg", Login = "poiu", Password = password };
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.Password), $"'{nameof(User.Password)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenFirstNameMoreThan50_ThenThrow()
        {
            //Arrange
            var name = new string('1', 51);
            var user = new User { FirstName = name, LastName = "Tremonin", Login = "VTr109", Email = "VTr@gmail.com", Password = "VTr9087" };
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.FirstName), $"The length of '{nameof(User.FirstName)}' must be 50 characters or fewer. You entered 51 characters.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenLastNameMoreThan50_ThenThrow()
        {
            //Arrange
            var name = new string('1', 51);
            var user = new User { FirstName = "Vli", LastName = name, Login = "VTr109", Email = "VTr@gmail.com", Password = "kjhf123" };
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.LastName), $"The length of '{nameof(User.LastName)}' must be 50 characters or fewer. You entered 51 characters.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenEmailMoreThan100_ThenThrow()
        {
            //Arrange
            var email = new string('1', 101);
            var user = new User { FirstName = "jhgd", LastName = "OIUY", Email = email, Login = "VTr109", Password = "kjhf123" };
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.Email), $"The length of '{nameof(User.Email)}' must be 100 characters or fewer. You entered 101 characters.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenLoginMoreThan100_ThenThrow()
        {
            //Arrange
            var login = new string('1', 101);
            var user = new User { FirstName = "jhgd", LastName = "OIUY", Email = "kjhgf", Login = login, Password = "kjhfs"};
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.Login), $"The length of '{nameof(User.Login)}' must be 100 characters or fewer. You entered 101 characters.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenPasswordMoreThan100_ThenThrow()
        {
            //Arrange
            var password = new string('1', 101);
            var user = new User { FirstName = "jhgd", LastName = "OIUY", Email = "kjhgf", Login = "lkjyr", Password = password };
            var validator = new UserValidator();
            var error = ValidationHelper.WrapError(nameof(User.Password), $"The length of '{nameof(User.Password)}' must be 100 characters or fewer. You entered 101 characters.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(user));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenModelIsRight_ThenPass()
        {
            //Arrange
            var user = new User { FirstName = "Vli", LastName = "Tremonin", Login = "VTr109", Email = "VTr@gmail.com", Password = "VTr9087"};
            var validator = new UserValidator();

            //Act
            validator.ValidateAndThrow(user);

            //Assert
            Assert.True(true);
        }
    }
}
