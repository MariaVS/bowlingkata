﻿using BoardApp.BLL.Tests.Helpers;
using BoardApp.BLL.Validators;
using BoardApp.Common.Models;
using FluentValidation;
using Xunit;

namespace BoardApp.BLL.Tests.Validators
{
    public class LabelValidatorTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenNameIsNullOrEmpty_ThenThrow(string name)
        {
            //Arrange
            var label = new Label { Name = name };
            var validator = new LabelValidator();
            var error = ValidationHelper.WrapError(nameof(Label.Name), $"'{nameof(Label.Name)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(label));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenNameMoreThan30_ThenThrow()
        {
            //Arrange
            var name = new string('1', 31);
            var label = new Label { Name = name };
            var validator = new LabelValidator();
            var error = ValidationHelper.WrapError(nameof(Label.Name), $"The length of '{nameof(Label.Name)}' must be 30 characters or fewer. You entered 31 characters.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(label));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenModelIsRight_ThenPass()
        {
            //Arrange
            var label = new Label { Name = "new"};
            var validator = new LabelValidator();

            //Act
            validator.ValidateAndThrow(label);

            //Assert
            Assert.True(true);
        }
    }
}
