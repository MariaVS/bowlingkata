﻿using BoardApp.BLL.Tests.Helpers;
using BoardApp.BLL.Validators;
using BoardApp.Common.Models;
using FluentValidation;
using Xunit;

namespace BoardApp.BLL.Tests.Validators
{
    public class CardValidatorTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenTitleIsNullOrEmpty_ThenThrow(string title)
        {
            //Arrange
            var card = new Card {Title = title};
            var validator = new CardValidator();
            var error = ValidationHelper.WrapError(nameof(Card.Title), $"'{nameof(Card.Title)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(card));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenModelIsRight_ThenPass()
        {
            //Arrange
            var card = new Card { Title = "new" };
            var validator = new CardValidator();

            //Act
            validator.ValidateAndThrow(card);

            //Assert
            Assert.True(true);
        }
    }
}
