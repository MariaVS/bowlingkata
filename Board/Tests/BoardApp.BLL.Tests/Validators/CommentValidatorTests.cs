﻿using System;
using BoardApp.BLL.Tests.Helpers;
using BoardApp.BLL.Validators;
using BoardApp.Common.Models;
using FluentValidation;
using Xunit;

namespace BoardApp.BLL.Tests.Validators
{
    public class CommentValidatorTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_WhenTextIsNullOrEmpty_ThenThrow(string title)
        {
            //Arrange
            var comment = new Comment { Text = title, DateTime = DateTime.Now };
            var validator = new CommentValidator();
            var error = ValidationHelper.WrapError(nameof(Comment.Text), $"'{nameof(Comment.Text)}' must not be empty.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(comment));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenDateTimeMinValue_ThenThrow()
        {
            //Arrange
            var comment = new Comment { Text = "new" };
            var validator = new CommentValidator();
            var error = ValidationHelper.WrapError(nameof(Comment.DateTime), $"'{nameof(Comment.DateTime)}' must be greater than '{DateTime.MinValue}'.");

            //Act & Assert
            var ex = Assert.Throws<ValidationException>(() => validator.ValidateAndThrow(comment));
            Assert.Equal(error, ex.Message);
        }

        [Fact]
        public void Validate_WhenModelIsRight_ThenPass()
        {
            //Arrange
            var comment = new Comment { Text = "new", DateTime = DateTime.Now };
            var validator = new CommentValidator();

            //Act
            validator.ValidateAndThrow(comment);

            //Assert
            Assert.True(true);
        }
    }
}
