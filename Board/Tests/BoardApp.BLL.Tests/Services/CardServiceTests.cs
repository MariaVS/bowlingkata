﻿using BoardApp.Common.Models;
using AutoMapper;
using BoardApp.BLL.Mappings;
using BoardApp.BLL.Services;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.DAL.Repositories;
using Moq;
using Xunit;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Tests.Services
{
    public class CardServiceTests
    {
        private Mock<IGenericRepository<Dal.Card>> _cardRepository;
        private IMapper _mapper;
        private Mock<IValidationService> _validationService;

        private ICardService _cardService;

        public CardServiceTests()
        {
            _cardRepository = new Mock<IGenericRepository<Dal.Card>>(MockBehavior.Strict);
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<ServicesProfile>()).CreateMapper();
            _validationService = new Mock<IValidationService>(MockBehavior.Strict);

            _cardService = new CardService(_cardRepository.Object, _mapper, _validationService.Object);
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var id = 4;
            var columnId = 6;
            var card = new Card { Id = id };
            _validationService.Setup(x => x.Validate<CardValidator, Card>(card));
            var dalCard = new Dal.Card { Id = id };
            _cardRepository.Setup(x => x.Add(It.IsAny<Dal.Card>()))
                .Callback<Dal.Card>(x =>
                {
                    Assert.Equal(id, x.Id);
                    Assert.Equal(columnId, x.ColumnId);
                }).Returns(dalCard);

            //Act
            var result = _cardService.Add(card, columnId);

            //Assert
            Assert.Equal(id, result.Id);
            _cardRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Delete()
        {
            //Arrange
            var id = 7;
            var dalCard = new Dal.Card() { Id = id };
            _cardRepository.Setup(x => x.Read(id)).Returns(dalCard);
            _cardRepository.Setup(x => x.Delete(dalCard));

            //Act 
            _cardService.Delete(id);

            //Assert
            _cardRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Update()
        {
            //Arrange
            var id = 10;
            var card = new Card { Id = id };
            _validationService.Setup(x => x.Validate<CardValidator, Card>(card));
            var dalCard = new Dal.Card() { Id = id };
            _cardRepository.Setup(x => x.Read(id)).Returns(dalCard);
            _cardRepository.Setup(x => x.Update(It.IsAny<Dal.Card>()))
                .Callback<Dal.Card>(x =>
                {
                    Assert.Equal(id, x.Id);
                }).Returns(dalCard);

            //Act
            var result = _cardService.Update(card);

            //Assert
            Assert.Equal(id, result.Id);
            _cardRepository.VerifyAll();
            _validationService.VerifyAll();
        }
    }
}
