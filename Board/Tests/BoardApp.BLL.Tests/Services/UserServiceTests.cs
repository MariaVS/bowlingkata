﻿using AutoMapper;
using BoardApp.BLL.Mappings;
using BoardApp.BLL.Services;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Moq;
using Xunit;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Tests.Services
{
    public class UserServiceTests
    {
        private Mock<IGenericRepository<Dal.User>> _userRepository;
        private IMapper _mapper;
        private Mock<IValidationService> _validationService;

        private IUserService _userService;

        public UserServiceTests()
        {
            _userRepository = new Mock<IGenericRepository<Dal.User>>(MockBehavior.Strict);
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<ServicesProfile>()).CreateMapper();
            _validationService = new Mock<IValidationService>(MockBehavior.Strict);

            _userService = new UserService(_userRepository.Object, _mapper, _validationService.Object);
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var id = 4;
            var user = new User { Id = id };
            _validationService.Setup(x => x.Validate<UserValidator, User>(user));
            var dalUser = new Dal.User() { Id = id };
            _userRepository.Setup(x => x.Add(It.IsAny<Dal.User>()))
                .Callback<Dal.User>(x =>
                {
                    Assert.Equal(id, x.Id);
                }).Returns(dalUser);

            //Act
            var result = _userService.Add(user);

            //Assert
            Assert.Equal(id, result.Id);
            _userRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Delete()
        {
            //Arrange
            var id = 7;
            var dalUser = new Dal.User() { Id = id };
            _userRepository.Setup(x => x.Read(id)).Returns(dalUser);
            _userRepository.Setup(x => x.Delete(dalUser));

            //Act 
            _userService.Delete(id);

            //Assert
            _userRepository.VerifyAll();
            _validationService.VerifyAll();
        }
    }
}
