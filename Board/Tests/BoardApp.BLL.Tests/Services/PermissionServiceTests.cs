﻿using AutoMapper;
using BoardApp.BLL.Mappings;
using BoardApp.BLL.Services;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Moq;
using Xunit;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Tests.Services
{
    public class PermissionServiceTests
    {
        private Mock<IGenericRepository<Dal.Permission>> _permissionRepository;
        private IMapper _mapper;
        private Mock<IValidationService> _validationService;

        private IPermissionService _permissionService;

        public PermissionServiceTests()
        {
            _permissionRepository = new Mock<IGenericRepository<Dal.Permission>>(MockBehavior.Strict);
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<ServicesProfile>()).CreateMapper();
            _validationService = new Mock<IValidationService>(MockBehavior.Strict);

            _permissionService = new PermissionService(_permissionRepository.Object, _mapper, _validationService.Object);
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var id = 4;
            var permission = new Permission { Id = id };
            _validationService.Setup(x => x.Validate<PermissionValidator, Permission>(permission));
            var dalPermission = new Dal.Permission() { Id = id };
            _permissionRepository.Setup(x => x.Add(It.IsAny<Dal.Permission>()))
                .Callback<Dal.Permission>(x =>
                {
                    Assert.Equal(id, x.Id);
                }).Returns(dalPermission);

            //Act
            var result = _permissionService.Add(permission);

            //Assert
            Assert.Equal(id, result.Id);
            _permissionRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Delete()
        {
            //Arrange
            var id = 7;
            var dalPermission = new Dal.Permission() { Id = id };
            _permissionRepository.Setup(x => x.Read(id)).Returns(dalPermission);
            _permissionRepository.Setup(x => x.Delete(dalPermission));

            //Act 
            _permissionService.Delete(id);

            //Assert
            _permissionRepository.VerifyAll();
            _validationService.VerifyAll();
        }
    }
}
