﻿using AutoMapper;
using BoardApp.BLL.Mappings;
using BoardApp.BLL.Services;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Moq;
using Xunit;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Tests.Services
{
    public class BoardServiceTests
    {
        private Mock<IGenericRepository<Dal.Board>> _boardRepository;
        private IMapper _mapper;
        private Mock<IValidationService> _validationService;

        private IBoardService _boardService;

        public BoardServiceTests()
        {
            _boardRepository = new Mock<IGenericRepository<Dal.Board>>(MockBehavior.Strict);
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<ServicesProfile>()).CreateMapper();
            _validationService = new Mock<IValidationService>(MockBehavior.Strict);

            _boardService = new BoardService(_boardRepository.Object, _mapper, _validationService.Object);
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var id = 4;
            var board = new Board { Id = id };
            _validationService.Setup(x => x.Validate<BoardValidator, Board>(board));
            var dalBoard = new Dal.Board() { Id = id };
            _boardRepository.Setup(x => x.Add(It.IsAny<Dal.Board>()))
                .Callback<Dal.Board>(x =>
                {
                    Assert.Equal(id, x.Id);
                }).Returns(dalBoard);

            //Act
            var result = _boardService.Add(board);

            //Assert
            Assert.Equal(id, result.Id);
            _boardRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Delete()
        {
            //Arrange
            var id = 7;
            var dalBoard = new Dal.Board() { Id = id };
            _boardRepository.Setup(x => x.Read(id)).Returns(dalBoard);
            _boardRepository.Setup(x => x.Delete(dalBoard));

            //Act 
             _boardService.Delete(id);

            //Assert
            _boardRepository.VerifyAll();
            _validationService.VerifyAll();
        }
    }
}
