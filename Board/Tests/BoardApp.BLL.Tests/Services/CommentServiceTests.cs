﻿using AutoMapper;
using BoardApp.BLL.Mappings;
using BoardApp.BLL.Services;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Moq;
using Xunit;
using Dal = BoardApp.DAL.Model;


namespace BoardApp.BLL.Tests.Services
{
    public class CommentServiceTests
    {
        private Mock<IGenericRepository<Dal.Comment>> _commentRepository;
        private IMapper _mapper;
        private Mock<IValidationService> _validationService;

        private ICommentService _commentService;

        public CommentServiceTests()
        {
            _commentRepository = new Mock<IGenericRepository<Dal.Comment>>(MockBehavior.Strict);
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<ServicesProfile>()).CreateMapper();
            _validationService = new Mock<IValidationService>(MockBehavior.Strict);

            _commentService = new CommentService(_commentRepository.Object, _mapper, _validationService.Object);
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var id = 4;
            var cardId = 6;
            var userId = 7;
            var comment = new Comment { Id = id };
            _validationService.Setup(x => x.Validate<CommentValidator, Comment>(comment));
            var dalComment = new Dal.Comment { Id = id };
            _commentRepository.Setup(x => x.Add(It.IsAny<Dal.Comment>()))
                .Callback<Dal.Comment>(x =>
                {
                    Assert.Equal(id, x.Id);
                    Assert.Equal(userId, x.UserId);
                    Assert.Equal(cardId, x.CardId);
                }).Returns(dalComment);

            //Act
            var result = _commentService.Add(comment, cardId, userId);

            //Assert
            Assert.Equal(id, result.Id);
            _commentRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Delete()
        {
            //Arrange
            var id = 7;
            var dalComment = new Dal.Comment { Id = id };
            _commentRepository.Setup(x => x.Read(id)).Returns(dalComment);
            _commentRepository.Setup(x => x.Delete(dalComment));

            //Act 
            _commentService.Delete(id);

            //Assert
            _commentRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Update()
        {
            //Arrange
            var id = 10;
            var comment = new Comment { Id = id };
            _validationService.Setup(x => x.Validate<CommentValidator, Comment>(comment));
            var dalComment = new Dal.Comment { Id = id };
            _commentRepository.Setup(x => x.Read(id)).Returns(dalComment);
            _commentRepository.Setup(x => x.Update(It.IsAny<Dal.Comment>()))
                .Callback<Dal.Comment>(x =>
                {
                    Assert.Equal(id, x.Id);
                }).Returns(dalComment);

            //Act
            var result = _commentService.Update(comment);

            //Assert
            Assert.Equal(id, result.Id);
            _commentRepository.VerifyAll();
            _validationService.VerifyAll();
        }
    }
}
