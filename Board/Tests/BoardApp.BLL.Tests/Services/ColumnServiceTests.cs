﻿using BoardApp.Common.Models;
using AutoMapper;
using BoardApp.BLL.Mappings;
using BoardApp.BLL.Services;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.DAL.Repositories;
using Moq;
using Xunit;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Tests.Services
{
    public class ColumnServiceTests
    {
        private Mock<IGenericRepository<Dal.Column>> _columnRepository;
        private IMapper _mapper;
        private Mock<IValidationService> _validationService;

        private IColumnService _columnService;

        public ColumnServiceTests()
        {
            _columnRepository = new Mock<IGenericRepository<Dal.Column>>(MockBehavior.Strict);
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<ServicesProfile>()).CreateMapper();
            _validationService = new Mock<IValidationService>(MockBehavior.Strict);

            _columnService = new ColumnService(_columnRepository.Object, _mapper, _validationService.Object);
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var id = 4;
            var boardId = 6;
            var column = new Column { Id = id };
            _validationService.Setup(x => x.Validate<ColumnValidator, Column>(column));
            var dalColumn = new Dal.Column { Id = id };
            _columnRepository.Setup(x => x.Add(It.IsAny<Dal.Column>()))
                .Callback<Dal.Column>(x =>
                {
                    Assert.Equal(id, x.Id);
                    Assert.Equal(boardId, x.BoardId);
                }).Returns(dalColumn);

            //Act
            var result = _columnService.Add(column, boardId);

            //Assert
            Assert.Equal(id, result.Id);
            _columnRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Delete()
        {
            //Arrange
            var id = 7;
            var dalColumn = new Dal.Column { Id = id };
            _columnRepository.Setup(x => x.Read(id)).Returns(dalColumn);
            _columnRepository.Setup(x => x.Delete(dalColumn));

            //Act 
            _columnService.Delete(id);

            //Assert
            _columnRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Update()
        {
            //Arrange
            var id = 10;
            var column = new Column { Id = id };
            _validationService.Setup(x => x.Validate<ColumnValidator, Column>(column));
            var dalColumn = new Dal.Column { Id = id };
            _columnRepository.Setup(x => x.Read(id)).Returns(dalColumn);
            _columnRepository.Setup(x => x.Update(It.IsAny<Dal.Column>()))
                .Callback<Dal.Column>(x =>
                {
                    Assert.Equal(id, x.Id);
                }).Returns(dalColumn);

            //Act
            var result = _columnService.Update(column);

            //Assert
            Assert.Equal(id, result.Id);
            _columnRepository.VerifyAll();
            _validationService.VerifyAll();
        }
    }
}
