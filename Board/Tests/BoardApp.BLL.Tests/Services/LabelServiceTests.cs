﻿using AutoMapper;
using BoardApp.BLL.Mappings;
using BoardApp.BLL.Services;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Moq;
using Xunit;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Tests.Services
{
    public class LabelServiceTests
    {
        private Mock<IGenericRepository<Dal.Label>> _labelRepository;
        private IMapper _mapper;
        private Mock<IValidationService> _validationService;

        private ILabelService _labelService;

        public LabelServiceTests()
        {
            _labelRepository = new Mock<IGenericRepository<Dal.Label>>(MockBehavior.Strict);
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<ServicesProfile>()).CreateMapper();
            _validationService = new Mock<IValidationService>(MockBehavior.Strict);

            _labelService = new LabelService(_labelRepository.Object, _mapper, _validationService.Object);
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var id = 4;
            var label = new Label { Id = id };
            _validationService.Setup(x => x.Validate<LabelValidator, Label>(label));
            var dalLabel = new Dal.Label() { Id = id };
            _labelRepository.Setup(x => x.Add(It.IsAny<Dal.Label>()))
                .Callback<Dal.Label>(x =>
                {
                    Assert.Equal(id, x.Id);
                }).Returns(dalLabel);

            //Act
            var result = _labelService.Add(label);

            //Assert
            Assert.Equal(id, result.Id);
            _labelRepository.VerifyAll();
            _validationService.VerifyAll();
        }

        [Fact]
        public void Delete()
        {
            //Arrange
            var id = 7;
            var dalLabel = new Dal.Label() { Id = id };
            _labelRepository.Setup(x => x.Read(id)).Returns(dalLabel);
            _labelRepository.Setup(x => x.Delete(dalLabel));

            //Act 
            _labelService.Delete(id);

            //Assert
            _labelRepository.VerifyAll();
            _validationService.VerifyAll();
        }
    }
}
