﻿using AutoMapper;
using BoardApp.BLL.Services;
using BoardApp.DAL.Repositories;
using BoardApp.Common.Models;
using BoardApp.BLL.Mappings;
using Moq;
using Xunit;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Tests.Services
{
    public class BoardAccessServiceTests
    {
        private Mock<IGenericRepository<Dal.BoardAccess>> _boardAccessRepository;
        private IMapper _mapper;

        private IBoardAccessService _boardAccessService;

        public BoardAccessServiceTests()
        {
            _boardAccessRepository = new Mock<IGenericRepository<Dal.BoardAccess>>(MockBehavior.Strict);
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<ServicesProfile>()).CreateMapper();

            _boardAccessService = new BoardAccessService(_boardAccessRepository.Object, _mapper);
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var id = 4;
            var userId = 5;
            var permissionId = 6;
            var boardId = 7;
            var boardAccess = new BoardAccess() { Id = id };
            var dalBoardAccess = new Dal.BoardAccess() { Id = id };
            _boardAccessRepository.Setup(x => x.Add(It.IsAny<Dal.BoardAccess>()))
                .Callback<Dal.BoardAccess>(x =>
                {
                    Assert.Equal(id, x.Id);
                    Assert.Equal(userId, x.UserId);
                    Assert.Equal(permissionId, x.PermissionId);
                    Assert.Equal(boardId, x.BoardId);
                }).Returns(dalBoardAccess);

            //Act
            var result = _boardAccessService.Add(boardAccess, userId, permissionId, boardId);

            //Assert
            Assert.Equal(id, result.Id);
            _boardAccessRepository.VerifyAll();
        }

        [Fact]
        public void Delete()
        {
            //Arrange
            var id = 7;
            var dalBoardAccess = new Dal.BoardAccess() { Id = id };
            _boardAccessRepository.Setup(x => x.Read(id)).Returns(dalBoardAccess);
            _boardAccessRepository.Setup(x => x.Delete(dalBoardAccess));

            //Act 
            _boardAccessService.Delete(id);

            //Assert
            _boardAccessRepository.VerifyAll();
        }

        [Fact]
        public void Update()
        {
            //Arrange
            var id = 10;
            var boardAccess = new BoardAccess() {Id = id};
            var dalBoardAccess = new Dal.BoardAccess() {Id = id};
            _boardAccessRepository.Setup(x => x.Read(id)).Returns(dalBoardAccess);
            _boardAccessRepository.Setup(x => x.Update(It.IsAny<Dal.BoardAccess>()))
                .Callback<Dal.BoardAccess>(x =>
                {
                    Assert.Equal(id, x.Id);
                }).Returns(dalBoardAccess);

            //Act
            var result = _boardAccessService.Update(boardAccess);

            //Assert
            Assert.Equal(id, result.Id);
            _boardAccessRepository.VerifyAll();
        }
    }
}
