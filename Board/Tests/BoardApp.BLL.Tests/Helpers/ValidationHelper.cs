﻿namespace BoardApp.BLL.Tests.Helpers
{
    public static class ValidationHelper
    {
        public static string WrapError(string propertyName, string error)
        {
            return $"Validation failed: \r\n -- {propertyName}: {error} Severity: Error"; ;
        }
    }
}
