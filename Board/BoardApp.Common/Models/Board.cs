﻿using System.Collections.Generic;

namespace BoardApp.Common.Models
{
    public class Board
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public IList<BoardAccess> BoardAccesses { get; set; }
        public IList<Column> Columns { get; set; }
    }
}
