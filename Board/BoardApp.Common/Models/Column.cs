﻿using System.Collections.Generic;

namespace BoardApp.Common.Models
{
    public class Column
    {
        public int Id { get; set; }
        public int BoardId { get; set; }
        public string Title { get; set; }
        public IList<Card> Cards { get; set; }
    }
}
