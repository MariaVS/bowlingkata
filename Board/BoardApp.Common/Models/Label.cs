﻿namespace BoardApp.Common.Models
{
    public class Label
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
