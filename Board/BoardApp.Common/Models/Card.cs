﻿using System.Collections.Generic;

namespace BoardApp.Common.Models
{
    public class Card
    {
        public int Id { get; set; }
        public int ColumnId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public IList<Label> Labels { get; set; }
        public IList<Comment> Comments { get; set; }
    }
}
