﻿using BoardApp.Common.Models;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BoardApp.DAL.Repositories;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Services
{
    public class BoardAccessService : IBoardAccessService
    {
        private readonly IGenericRepository<Dal.BoardAccess> _boardAccessRepository;
        private readonly IMapper _mapper;

        public BoardAccessService(IGenericRepository<Dal.BoardAccess> boardAccessRepository, IMapper mapper)
        {
            _boardAccessRepository = boardAccessRepository;
            _mapper = mapper;
        }

        public BoardAccess Add(BoardAccess entity, int userId, int permissionId, int boardId)
        {
            var boardAccess = _mapper.Map<Dal.BoardAccess>(entity);
            boardAccess.BoardId = boardId;
            boardAccess.PermissionId = permissionId;
            boardAccess.UserId = userId;
            var result = _boardAccessRepository.Add(boardAccess);
            return _mapper.Map<BoardAccess>(result);
        }

        public void Delete(int id)
        {
            var board = _boardAccessRepository.Read(id);
            _boardAccessRepository.Delete(board);
        }

        public BoardAccess Read(int id)
        {
            var result = _boardAccessRepository.Read(id);
            return _mapper.Map<BoardAccess>(result);
        }

        public IList<BoardAccess> ReadAll()
        {
            var result = _boardAccessRepository.ReadAll().ToList();
            return _mapper.Map<List<BoardAccess>>(result);
        }

        public BoardAccess Update(BoardAccess entity)
        {
            var current = _boardAccessRepository.Read(entity.Id);
            _mapper.Map(entity, current);
            var result = _boardAccessRepository.Update(current);
            return _mapper.Map<BoardAccess>(result);
        }
    }
}
