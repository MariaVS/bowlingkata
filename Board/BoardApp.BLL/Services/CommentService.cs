﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly IGenericRepository<Dal.Comment> _commentRepository;
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;

        public CommentService(IGenericRepository<Dal.Comment> commentRepository, IMapper mapper, IValidationService validationService)
        {
            _commentRepository = commentRepository;
            _mapper = mapper;
            _validationService = validationService;
        }

        public Comment Add(Comment entity, int cardId, int userId)
        {
            _validationService.Validate<CommentValidator, Comment>(entity);
            var comment = _mapper.Map<Dal.Comment>(entity);
            comment.CardId = cardId;
            comment.UserId = userId;
            var result = _commentRepository.Add(comment);
            return _mapper.Map<Comment>(result);
        }

        public void Delete(int id)
        {
            var comment = _commentRepository.Read(id);
            _commentRepository.Delete(comment);
        }

        public Comment Read(int id)
        {
            var result = _commentRepository.Read(id);
            return _mapper.Map<Comment>(result);
        }

        public IList<Comment> ReadAll()
        {
            var result = _commentRepository.ReadAll().ToList();
            return _mapper.Map<List<Comment>>(result);
        }

        public Comment Update(Comment entity)
        {
            _validationService.Validate<CommentValidator, Comment>(entity);
            var current = _commentRepository.Read(entity.Id);
            _mapper.Map(entity, current);
            var result = _commentRepository.Update(current);
            return _mapper.Map<Comment>(result);
        }
    }
}
