﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IGenericRepository<Dal.User> _userRepository;
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;

        public UserService(IGenericRepository<Dal.User> userRepository, IMapper mapper, IValidationService validationService)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _validationService = validationService;
        }

        public User Add(User entity)
        {
            _validationService.Validate<UserValidator, User>(entity);
            var user = _mapper.Map<Dal.User>(entity);
            var result = _userRepository.Add(user);
            return _mapper.Map<User>(result);
        }

        public void Delete(int id)
        {
            var permission = _userRepository.Read(id);
            _userRepository.Delete(permission);
        }

        public User Read(int id)
        {
            var result = _userRepository.Read(id);
            return _mapper.Map<User>(result);
        }

        public IList<User> ReadAll()
        {
            var result = _userRepository.ReadAll().ToList();
            return _mapper.Map<List<User>>(result);
        }

        public User Update(User entity)
        {
            _validationService.Validate<UserValidator, User>(entity);
            var current = _userRepository.Read(entity.Id);
            _mapper.Map(entity, current);
            var result = _userRepository.Update(current);
            return _mapper.Map<User>(result);
        }
    }
}
