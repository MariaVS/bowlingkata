﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Services
{
    public class PermissionService : IPermissionService
    {
        private readonly IGenericRepository<Dal.Permission> _permissionRepository;
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;

        public PermissionService(IGenericRepository<Dal.Permission> permissionRepository, IMapper mapper, IValidationService validationService)
        {
            _permissionRepository = permissionRepository;
            _mapper = mapper;
            _validationService = validationService;
        }

        public Permission Add(Permission entity)
        {
            _validationService.Validate<PermissionValidator, Permission>(entity);
            var permission = _mapper.Map<Dal.Permission>(entity);
            var result = _permissionRepository.Add(permission);
            return _mapper.Map<Permission>(result);
        }

        public void Delete(int id)
        {
            var permission = _permissionRepository.Read(id);
            _permissionRepository.Delete(permission);
        }

        public Permission Read(int id)
        {
            var result = _permissionRepository.Read(id);
            return _mapper.Map<Permission>(result);
        }

        public IList<Permission> ReadAll()
        {
            var result = _permissionRepository.ReadAll().ToList();
            return _mapper.Map<List<Permission>>(result);
        }

        public Permission Update(Permission entity)
        {
            _validationService.Validate<PermissionValidator, Permission>(entity);
            var current = _permissionRepository.Read(entity.Id);
            _mapper.Map(entity, current);
            var result = _permissionRepository.Update(current);
            return _mapper.Map<Permission>(result);
        }
    }
}
