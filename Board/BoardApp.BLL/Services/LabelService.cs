﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Services
{
    public class LabelService : ILabelService
    {
        private readonly IGenericRepository<Dal.Label> _labelRepository;
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;

        public LabelService(IGenericRepository<Dal.Label> labelRepository, IMapper mapper, IValidationService validationService)
        {
            _labelRepository = labelRepository;
            _mapper = mapper;
            _validationService = validationService;
        }

        public Label Add(Label entity)
        {
            _validationService.Validate<LabelValidator, Label>(entity);
            var label = _mapper.Map<Dal.Label>(entity);
            var result = _labelRepository.Add(label);
            return _mapper.Map<Label>(result);
        }

        public void Delete(int id)
        {
            var label = _labelRepository.Read(id);
            _labelRepository.Delete(label);
        }

        public Label Read(int id)
        {
            var result = _labelRepository.Read(id);
            return _mapper.Map<Label>(result);
        }

        public IList<Label> ReadAll()
        {
            var result = _labelRepository.ReadAll().ToList();
            return _mapper.Map<List<Label>>(result);
        }

        public Label Update(Label entity)
        {
            _validationService.Validate<LabelValidator, Label>(entity);
            var current = _labelRepository.Read(entity.Id);
            _mapper.Map(entity, current);
            var result = _labelRepository.Update(current);
            return _mapper.Map<Label>(result);
        }
    }
}
