﻿using BoardApp.Common.Models;

namespace BoardApp.BLL.Services
{
    public interface ILabelService : IGenericService<Label>
    {
        Label Add(Label entity);
    }
}
