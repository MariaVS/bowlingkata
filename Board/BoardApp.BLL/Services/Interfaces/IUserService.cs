﻿using BoardApp.Common.Models;

namespace BoardApp.BLL.Services
{
    public interface IUserService : IGenericService<User>
    {
        User Add(User entity);
    }
}
