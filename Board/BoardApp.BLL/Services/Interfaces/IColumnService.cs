﻿using BoardApp.Common.Models;

namespace BoardApp.BLL.Services
{
    public interface IColumnService : IGenericService<Column>
    {
        Column Add(Column entity, int boardId);
    }
}