﻿using BoardApp.Common.Models;

namespace BoardApp.BLL.Services
{
    public interface IPermissionService : IGenericService<Permission>
    {
        Permission Add(Permission entity);
    }
}
