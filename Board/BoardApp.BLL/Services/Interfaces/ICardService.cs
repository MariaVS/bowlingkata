﻿using BoardApp.Common.Models;

namespace BoardApp.BLL.Services
{
    public interface ICardService : IGenericService<Card>
    {
        Card Add(Card entity, int columnId);
    }
}
