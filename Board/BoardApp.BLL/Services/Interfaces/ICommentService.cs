﻿using BoardApp.Common.Models;

namespace BoardApp.BLL.Services
{
    public interface ICommentService : IGenericService<Comment>
    {
        Comment Add(Comment entity, int cardId, int userId);
    }
}
