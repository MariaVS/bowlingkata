﻿using BoardApp.Common.Models;

namespace BoardApp.BLL.Services
{
    public interface IBoardAccessService : IGenericService<BoardAccess>
    {
        BoardAccess Add(BoardAccess entity, int userId, int permissionId, int boardId);
    }
}
