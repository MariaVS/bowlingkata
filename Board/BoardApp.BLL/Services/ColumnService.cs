﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using BoardApp.DAL.Repositories;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Services
{
    public class ColumnService : IColumnService
    {
        private readonly IGenericRepository<Dal.Column> _columnRepository;
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;

        public ColumnService(IGenericRepository<Dal.Column> columnRepository, IMapper mapper, IValidationService validationService)
        {
            _columnRepository = columnRepository;
            _mapper = mapper;
            _validationService = validationService;
        }

        public Column Add(Column entity, int boardId)
        {
            _validationService.Validate<ColumnValidator, Column>(entity);
            var column = _mapper.Map<Dal.Column>(entity);
            column.BoardId = boardId;
            var result = _columnRepository.Add(column);
            return _mapper.Map<Column>(result);
        }

        public void Delete(int id)
        {
            var column = _columnRepository.Read(id);
            _columnRepository.Delete(column);
        }

        public Column Read(int id)
        {
            var result = _columnRepository.Read(id);
            return _mapper.Map<Column>(result);
        }

        public IList<Column> ReadAll()
        {
            var result = _columnRepository.ReadAll().ToList();
            return _mapper.Map<List<Column>>(result);
        }

        public Column Update(Column entity)
        {
            _validationService.Validate<ColumnValidator, Column>(entity);
            var current = _columnRepository.Read(entity.Id);
            _mapper.Map(entity, current);
            var result = _columnRepository.Update(current);
            return _mapper.Map<Column>(result);
        }
    }
}
