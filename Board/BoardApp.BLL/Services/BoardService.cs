﻿using BoardApp.Common.Models;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.DAL.Repositories;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Services
{
    public class BoardService : IBoardService
    {
        private readonly IGenericRepository<Dal.Board> _boardRepository;
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;

        public BoardService(IGenericRepository<Dal.Board> boardRepository, IMapper mapper, IValidationService validationService)
        {
            _boardRepository = boardRepository;
            _mapper = mapper;
            _validationService = validationService;
        }

        public Board Add(Board entity)
        {
            _validationService.Validate<BoardValidator, Board>(entity);
            var board = _mapper.Map<Dal.Board>(entity);
            var result = _boardRepository.Add(board);
            return _mapper.Map<Board>(result);
        }

        public void Delete(int id)
        {
            var board = _boardRepository.Read(id);
            _boardRepository.Delete(board);
        }

        public Board Read(int id)
        {
            var result = _boardRepository.Read(id);
            return _mapper.Map<Board>(result);
        }

        public IList<Board> ReadAll()
        {
            var result = _boardRepository.ReadAll().ToList();
            return _mapper.Map<List<Board>>(result);
        }

        public Board Update(Board entity)
        {
            _validationService.Validate<BoardValidator, Board>(entity);
            var current = _boardRepository.Read(entity.Id);
            _mapper.Map(entity, current);
            var result = _boardRepository.Update(current);
            return _mapper.Map<Board>(result);
        }
    }
}
