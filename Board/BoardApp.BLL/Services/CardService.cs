﻿using BoardApp.Common.Models;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BoardApp.BLL.Validators;
using BoardApp.BLL.Validators.Base;
using BoardApp.DAL.Repositories;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Services
{
    public class CardService : ICardService
    {
        private readonly IGenericRepository<Dal.Card> _cardRepository;
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;

        public CardService(IGenericRepository<Dal.Card> cardRepository, IMapper mapper, IValidationService validationService)
        {
            _cardRepository = cardRepository;
            _mapper = mapper;
            _validationService = validationService;
        }

        public Card Add(Card entity, int columnId)
        {
            _validationService.Validate<CardValidator, Card>(entity);
            var card = _mapper.Map<Dal.Card>(entity);
            card.ColumnId = columnId;
            var result = _cardRepository.Add(card);
            return _mapper.Map<Card>(result);
        }

        public void Delete(int id)
        {
            var card = _cardRepository.Read(id);
            _cardRepository.Delete(card);
        }

        public Card Read(int id)
        {
            var result = _cardRepository.Read(id);
            return _mapper.Map<Card>(result);
        }

        public IList<Card> ReadAll()
        {
            var result = _cardRepository.ReadAll().ToList();
            return _mapper.Map<List<Card>>(result);
        }

        public Card Update(Card entity)
        {
            _validationService.Validate<CardValidator, Card>(entity);
            var current = _cardRepository.Read(entity.Id);
            _mapper.Map(entity, current);
            var result = _cardRepository.Update(current);
            return _mapper.Map<Card>(result);
        }
    }
}
