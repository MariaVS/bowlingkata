﻿using BoardApp.BLL.Services;
using BoardApp.BLL.Validators.Base;
using Microsoft.Extensions.DependencyInjection;

namespace BoardApp.BLL.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddServicesServices(this IServiceCollection services)
        {
            services.AddTransient<IBoardService, BoardService>();
            services.AddTransient<ICardService, CardService>();
            services.AddTransient<IBoardAccessService, BoardAccessService>();
            services.AddTransient<IColumnService, ColumnService>();
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<ILabelService, LabelService>();
            services.AddTransient<IPermissionService, PermissionService>();
            services.AddTransient<IUserService, UserService>();
        }

        public static void AddValidationServices(this IServiceCollection services)
        {
            services.AddTransient<IValidationService, ValidationService>();
        }
    }
}
