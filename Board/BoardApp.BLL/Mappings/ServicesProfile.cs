﻿using AutoMapper;
using BoardApp.Common.Models;
using Dal = BoardApp.DAL.Model;

namespace BoardApp.BLL.Mappings
{
    public class ServicesProfile : Profile
    {
        public ServicesProfile()
        {
            CreateMap<Board, Dal.Board>().ReverseMap();
            CreateMap<BoardAccess, Dal.BoardAccess>().ReverseMap();
            CreateMap<Card, Dal.Card>().ReverseMap();
            CreateMap<Column, Dal.Column>().ReverseMap();
            CreateMap<Comment, Dal.Comment>().ReverseMap();
            CreateMap<Label, Dal.Label>().ReverseMap();
            CreateMap<Permission, Dal.Permission>().ReverseMap();
            CreateMap<User, Dal.User>().ReverseMap();
        }
    }
}
