﻿using FluentValidation;

namespace BoardApp.BLL.Validators.Base
{
    public class ValidationService : IValidationService
    {
        public void Validate<TValidator, T>(T entity) where TValidator : BaseValidator<T>, new()
        {
            TValidator validator = new TValidator();
            validator.ValidateAndThrow(entity);
        }
    }
}
