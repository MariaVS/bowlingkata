﻿namespace BoardApp.BLL.Validators.Base
{
    public interface IValidationService
    {
        void Validate<TValidator, T>(T entity) where TValidator : BaseValidator<T>, new();
    }
}
