﻿using System;
using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using FluentValidation;

namespace BoardApp.BLL.Validators
{
    public class CommentValidator : BaseValidator<Comment>
    {
        public CommentValidator()
        {
            RuleFor(x => x.Text).NotEmpty();
            RuleFor(x => x.DateTime).GreaterThan(DateTime.MinValue);
        }
    }
}
