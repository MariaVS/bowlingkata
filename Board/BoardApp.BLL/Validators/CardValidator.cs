﻿using BoardApp.BLL.Validators.Base;
using BoardApp.Common.Models;
using FluentValidation;

namespace BoardApp.BLL.Validators
{
    public class CardValidator : BaseValidator<Card>
    {
        public CardValidator()
        {
            RuleFor(x => x.Title).NotEmpty();
        }
    }
}
