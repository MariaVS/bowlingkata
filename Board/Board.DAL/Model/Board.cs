﻿using System.Collections.Generic;

namespace BoardApp.DAL.Model
{
    public class Board
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual HashSet<BoardAccess> BoardAccesses { get; set; }
        public virtual HashSet<Column> Columns { get; set; }
    }
}
