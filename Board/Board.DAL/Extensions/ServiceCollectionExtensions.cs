﻿using System.Diagnostics;
using BoardApp.DAL.Model;
using BoardApp.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BoardApp.DAL.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDbContextServices(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<BoardDbContext>(builder =>
            {
                builder.EnableSensitiveDataLogging();
                builder.LogTo(message => Debug.WriteLine(message));
                builder.UseLazyLoadingProxies().UseSqlServer(connectionString);
            });
        }

        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddTransient<IGenericRepository<Board>, GenericRepository<Board>>();
            services.AddTransient<IGenericRepository<BoardAccess>, GenericRepository<BoardAccess>>();
            services.AddTransient<IGenericRepository<Card>, GenericRepository<Card>>();
            services.AddTransient<IGenericRepository<Column>, GenericRepository<Column>>();
            services.AddTransient<IGenericRepository<Comment>, GenericRepository<Comment>>();
            services.AddTransient<IGenericRepository<Label>, GenericRepository<Label>>();
            services.AddTransient<IGenericRepository<Permission>, GenericRepository<Permission>>();
            services.AddTransient<IGenericRepository<User>, GenericRepository<User>>();
        }
    }
}
