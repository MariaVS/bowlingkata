﻿using BoardApp.BLL.Extensions;
using BoardApp.BLL.Mappings;
using BoardApp.DAL.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BoardApp.WebApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services, IConfiguration configuration)
        {
            AddBllServices(services);
            AddDalServices(services, configuration);
            AddAutomapperServices(services);
        }

        private static void AddAutomapperServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(ServicesProfile));
        }

        private static void AddBllServices(IServiceCollection services)
        {
            services.AddServicesServices();
            services.AddValidationServices();
        }

        private static void AddDalServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContextServices(configuration.GetConnectionString("DefaultConnection"));
            services.AddRepositoryServices();
        }
    }
}
