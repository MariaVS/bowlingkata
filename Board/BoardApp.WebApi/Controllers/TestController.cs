﻿using System.Collections.Generic;
using BoardApp.BLL.Services;
using BoardApp.Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace BoardApp.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {
        private readonly ICardService _cardService;
        private readonly IBoardService _boardService;
        private readonly IUserService _userService;

        public TestController(ICardService cardService, IBoardService boardService, IUserService userService)
        {
            _cardService = cardService;
            _boardService = boardService;
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<IList<User>> GetUsers()
        {
            var result = _userService.ReadAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public ActionResult<User> GetUser(int id)
        {
            var result = _userService.Read(id);
            return Ok(result);
        }

        [HttpGet("cards/{id}")]
        public ActionResult<Card> GetCard(int id)
        {
            var result = _cardService.Read(id);
            return Ok(result);
        }
    }
}
