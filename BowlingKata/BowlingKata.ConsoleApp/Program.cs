﻿using System;

namespace BowlingKata.ConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            PointsCounter pointsCounter = new PointsCounter();
            pointsCounter.Count(1);
            pointsCounter.Count(4);

            pointsCounter.Count(4);
            pointsCounter.Count(5);

            pointsCounter.Count(6);
            pointsCounter.Count(4);

            pointsCounter.Count(5);
            pointsCounter.Count(5);

            pointsCounter.Count(10);

            pointsCounter.Count(0);
            pointsCounter.Count(1);

            pointsCounter.Count(7);
            pointsCounter.Count(3);

            pointsCounter.Count(6);
            pointsCounter.Count(4);

            pointsCounter.Count(10);

            pointsCounter.Count(2);
            pointsCounter.Count(8);
            pointsCounter.Count(6);

            var result = pointsCounter.ToString();
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
