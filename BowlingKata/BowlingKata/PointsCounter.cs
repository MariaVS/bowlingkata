﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BowlingKata
{
    public class PointsCounter
    {
        private int _opportunityCounter = 1;
        public int OpportunityCounter => _opportunityCounter;
        private int _frameCounter = 1;
        public int FrameCounter => _frameCounter;
        private List<Frame> _frames = new List<Frame>();

        public PointsCounter()
        {
            for (int i = 0; i < 10; i++)
            {
                _frames.Add(new Frame());
            }
        }

        public void Count(int points)
        {
            CheckFrame();
            if (points > 10 || points < 0)
            {
                throw new ArgumentException("Points can't be less than 0 and more than 10");
            }

            AddPoints(points);
            ResolveSpareOrStrike();
            IncreaseOpportunity();
        }

        private void CheckFrame()
        {
            if (_frameCounter > 10)
            {
                throw new ApplicationException("Frame can't be more than 10");
            }
        }

        private void AddPoints(int points)
        {
            var currentFrame = _frames[_frameCounter - 1];
            CheckSumOfFrame(currentFrame, points);
            currentFrame.Opportunities.Add(points);
            AddPointsFromCurrentToLastFrame(points, _frameCounter);
            CheckStrikeOrSpare(currentFrame);
        }

        private void CheckSumOfFrame(Frame frame, int points)
        {
            if (_opportunityCounter == 2 && !frame.IsExtraOpportunity)
            {
                var pointsOfFirstOpportunity = frame.Opportunities.First();
                if (pointsOfFirstOpportunity + points > 10)
                {
                    var delta = 10 - pointsOfFirstOpportunity;
                    throw new ArgumentException(
                        $"First opportunity: {pointsOfFirstOpportunity} points, points of the second opportunity can't be more than {delta}");
                }
            }

            if (_opportunityCounter >= 2 && frame.IsExtraOpportunity)
            {
                var isLastStrike = frame.Opportunities.Last() == 10;
                if (isLastStrike && _opportunityCounter >= 3)
                {
                    return;
                }

                if (_opportunityCounter == 3 && frame.Opportunities.Sum() == 10)
                {
                    return;
                }

                if (!isLastStrike && _opportunityCounter >= 3)
                {
                    var pointsOfLastOpportunity = frame.Opportunities.Last();
                    if (pointsOfLastOpportunity < 10)
                    {
                        var delta = 10 - pointsOfLastOpportunity;

                        if (points > delta)
                        {
                            throw new ArgumentException(
                                $"Previous opportunity: {pointsOfLastOpportunity} points, points of the current opportunity can't be more than {delta}");
                        }
                    }
                }
            }
        }

        private void CheckStrikeOrSpare(Frame frame)
        {
            if (frame.Opportunities.Count == 1 && frame.Opportunities[0] == 10)
            {
                if (_frameCounter == 10)
                {
                    frame.IsExtraOpportunity = true;
                }
                else
                {
                    frame.IsStrikeResolved = false;
                }
            }

            if (frame.Opportunities.Count == 2 && frame.Opportunities.Sum() == 10)
            {
                if (_frameCounter == 10)
                {
                    frame.IsExtraOpportunity = true;
                }
                else
                {
                    frame.IsSpareResolved = false;
                }
            }
        }

        private void AddPointsFromCurrentToLastFrame(int points, int frameId)
        {
            for (int i = frameId; i <= 10; i++)
            {
                _frames[i - 1].Sum += points;
            }
        }

        private void IncreaseOpportunity()
        {
            if (!_frames[_frameCounter - 1].IsStrikeResolved)
            {
                _frameCounter++;
                _opportunityCounter = 1;
                return;
            }

            _opportunityCounter++;
            if (_opportunityCounter >= 3)
            {
                if (_opportunityCounter == 3 && _frames[_frameCounter - 1].IsExtraOpportunity)
                {
                    return;
                }

                _opportunityCounter = 1;
                _frameCounter++;
            }
        }

        public int GetSumByFrame(int frameId)
        {
            return _frames[frameId - 1].Sum;
        }

        private void ResolveSpareOrStrike()
        {
            for (int i = 0; i < 9; i++)
            {
                var currentFrame = _frames[i];
                if (!currentFrame.IsSpareResolved)
                {
                    if (!ResolveSpare(i))
                    {
                        return;
                    }
                }

                if (!currentFrame.IsStrikeResolved)
                {
                    if (!ResolveStrike(i))
                    {
                        return;
                    }
                }
            }
        }

        private bool ResolveSpare(int i)
        {
            var currentFrame = _frames[i];
            var nextFrame = _frames[i + 1];
            if (nextFrame.Opportunities.Count > 0)
            {
                var bonus = nextFrame.Opportunities.First();
                AddPointsFromCurrentToLastFrame(bonus, i + 1);
                currentFrame.IsSpareResolved = true;
            }

            return currentFrame.IsSpareResolved;
        }

        private bool ResolveStrike(int i)
        {
            var currentFrame = _frames[i];
            var nextFrame = _frames[i + 1];
            if (nextFrame.Opportunities.Count >= 2)
            {
                var bonus = nextFrame.Opportunities[0] + nextFrame.Opportunities[1];
                AddPointsFromCurrentToLastFrame(bonus, i + 1);
                currentFrame.IsStrikeResolved = true;
            }
            else if (nextFrame.Opportunities.Count == 1 && i < 8)
            {
                var nextNextFrame = _frames[i + 2];
                if (nextNextFrame.Opportunities.Count > 0)
                {
                    var bonus = nextFrame.Opportunities.First() + nextNextFrame.Opportunities.First();
                    AddPointsFromCurrentToLastFrame(bonus, i + 1);
                    currentFrame.IsStrikeResolved = true;
                }
            }

            return currentFrame.IsStrikeResolved;
        }

        public override string ToString()
        {
            string show = string.Empty;
            foreach (var frame in _frames)
            {
                string frameStr = $"{frame.Sum}  ";
                foreach (var opportunity in frame.Opportunities)
                {
                    frameStr += $"|{opportunity}";
                }

                frameStr += "|\n";
                show += frameStr;
            }

            return show;
        }
    }
}