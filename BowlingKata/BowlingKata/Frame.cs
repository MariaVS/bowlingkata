﻿using System.Collections.Generic;

namespace BowlingKata
{
    public class Frame
    {
        public List<int> Opportunities { get; set; } = new List<int>();
        public int Sum { get; set; }
        public bool IsSpareResolved { get; set; } = true;
        public bool IsStrikeResolved { get; set; } = true;
        public bool IsExtraOpportunity { get; set; }
    }
}
