using System;
using Xunit;

namespace BowlingKata.Tests
{
    public class PointsCounterTests
    {
        [Theory]
        [InlineData(-1)]
        [InlineData(11)]
        public void Count_WhenScoredLessThanZeroOrMoreThanTen_ThenThrowException(int points)
        {
            //Arrange
            var pointsCounter = new PointsCounter();

            //Act & Assert 
            var error = Assert.Throws<ArgumentException>(() => pointsCounter.Count(points));
            Assert.Equal("Points can't be less than 0 and more than 10", error.Message);
        }

        [Fact]
        public void Count_WhenMoreThanTenFrame_ThenThrowException()
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, 10);
            pointsCounter.Count(4);
            pointsCounter.Count(4);

            //Act & Assert 
            var error = Assert.Throws<ApplicationException>(() => pointsCounter.Count(4));
            Assert.Equal("Frame can't be more than 10", error.Message);
        }

        [Theory]
        [InlineData(5, 6)]
        public void Count_WhenSumOfTwoOpportunitiesMoreThanTenPoints_ThenThrowException(int points1, int points2)
        { 
            //Arrange
            var pointsCounter = new PointsCounter();
            pointsCounter.Count(points1);

            //Act & Assert 
            var error = Assert.Throws<ArgumentException>(() => pointsCounter.Count(points2));
            Assert.Equal($"First opportunity: {points1} points, points of the second opportunity can't be more than {10 - points1}", error.Message);
        }

        [Theory]
        [InlineData(6, 6, 10)]
        public void Count_WhenTenFrameStrikeAndTwoOpportunityNoStrikeAndSumOfTwoOpportunitiesMoreThanTen_ThenThrowException(int points1, int points2, int points)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, 10);
            pointsCounter.Count(points);
            pointsCounter.Count(points1);
            var result = 10 - points1;

            //Act & Assert
            var error = Assert.Throws<ArgumentException>(() => pointsCounter.Count(points2));
            Assert.Equal($"Previous opportunity: {points1} points, points of the current opportunity can't be more than {result}", error.Message);
        }

        [Theory]
        [InlineData(2, 6)]
        [InlineData(9, 6)]
        public void Count_WhenScoredInFrame_ThenAddPointsToThisFrame(int frameId, int points)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, frameId);
            var currentSum = pointsCounter.GetSumByFrame(frameId);

            //Act
            pointsCounter.Count(points);
            var resultSum = pointsCounter.GetSumByFrame(frameId);

            //Assert
            Assert.Equal(points, resultSum - currentSum);
        }

        [Theory]
        [InlineData(7, 2, 4)]
        public void Count_WhenFinishedTwoOpportunities_NoStrikeNoSpare_ThenFrameIsSumTwoOpportunities(int points1, int points2, int frameId)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, frameId);

            var currentSum = pointsCounter.GetSumByFrame(frameId);

            //Act
            pointsCounter.Count(points1);
            pointsCounter.Count(points2);
            var resultSum = pointsCounter.GetSumByFrame(frameId);

            //Assert
            Assert.Equal(points1 + points2, resultSum - currentSum);
        }

        [Theory]
        [InlineData(2, 9)]
        [InlineData(6, 9)]
        public void Count_WhenSpare_ThenFrameIsSumTenAndNextOpportunity(int frameId, int points)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, frameId);
            var sum = pointsCounter.GetSumByFrame(frameId);
            pointsCounter.Count(8);
            pointsCounter.Count(2);

            //Act
            pointsCounter.Count(points);
            var resultSum = pointsCounter.GetSumByFrame(frameId);

            //Assert
            Assert.Equal(10 + points, resultSum - sum);
        }

        [Theory]
        [InlineData(10)]
        public void Count_WhenStrike_ThenMoveToNextFrame(int points)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            var frameCounter = pointsCounter.FrameCounter;

            //Act
            pointsCounter.Count(points);
            var resultFrameCounter = pointsCounter.FrameCounter;

            //Assert
            Assert.Equal(frameCounter + 1, resultFrameCounter);
        }

        [Theory]
        [InlineData(2, 5, 3)]
        [InlineData(6,5, 3)]
        public void Count_WhenStrike_ThenFrameIsSumTenAndNextTwoOpportunities(int frameId, int points1, int points2)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, frameId);
            var sumFrame = pointsCounter.GetSumByFrame(frameId);
            pointsCounter.Count(10);

            //Act
            pointsCounter.Count(points1);
            pointsCounter.Count(points2);
            var resultSum = pointsCounter.GetSumByFrame(frameId);

            //Assert
            Assert.Equal(points1 + points2 + 10, resultSum - sumFrame);
        }

        [Theory]
        [InlineData(2, 6)]
        [InlineData(6, 6)]
        public void Count_WhenStrikeAndStrike_ThenFrameIsSumTwentyAndNextOpportunity(int frameId, int points)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, frameId);
            var sumFrame = pointsCounter.GetSumByFrame(frameId);
            pointsCounter.Count(10);

            //Act
            pointsCounter.Count(10);
            pointsCounter.Count(points);
            var resultSum = pointsCounter.GetSumByFrame(frameId);

            //Assert
            Assert.Equal(points + 20, resultSum - sumFrame);
        }

        [Theory]
        [InlineData(4, 6, 7)]
        public void Count_WhenTenFrameSpare_ThenFrameIsSumTheeOpportunities(int points1, int points2, int points)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, 10);
            var sum = pointsCounter.GetSumByFrame(10);
            pointsCounter.Count(points1);
            pointsCounter.Count(points2);

            //Act
            pointsCounter.Count(points);
            var resultSum = pointsCounter.GetSumByFrame(10);

            //Assert
            Assert.Equal(points + points1 + points2, resultSum - sum);
        }

        [Theory]
        [InlineData(6, 3, 10)]
        public void Count_WhenTenFrameStrike_ThenFrameIsSumTheeOpportunities(int points1, int points2, int points)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, 10);
            var sum = pointsCounter.GetSumByFrame(10);
            pointsCounter.Count(points);

            //Act
            pointsCounter.Count(points1);
            pointsCounter.Count(points2);
            var sumResult = pointsCounter.GetSumByFrame(10);

            //Assert
            Assert.Equal(points + points1 + points2, sumResult - sum);
        }

        [Theory]
        [InlineData(10, 10, 10)]
        public void Count_WhenTenFrameIsThreeStrike_ThenSumTenFrameIsThirty(int points1, int points2, int points3)
        {
            //Arrange
            var pointsCounter = new PointsCounter();
            SkipToFrame(pointsCounter, 10);
            var sum = pointsCounter.GetSumByFrame(10);
            pointsCounter.Count(points1);
            pointsCounter.Count(points2);
            //Act
            pointsCounter.Count(points3);
            var sumResult = pointsCounter.GetSumByFrame(10);

            //Assert
            Assert.Equal(30, sumResult - sum);
        }

        private void SkipToFrame(PointsCounter pointsCounter, int frameId)
        {
            while (pointsCounter.FrameCounter < frameId)
            {
                pointsCounter.Count(4);
            }
        }
    }
}
